//
// Created by kikyy_99 on 05.01.23..
//

#include "SA_IS_FMIndex.h"
#include <execution>
#include <algorithm>
#include <ranges>
#include <type_traits>
#include <iostream>
#include <sstream>

using namespace std;

void SA_IS_FMIndex::constructFMIndex() {
    /*
     * Prepare and initialise data structures
     */
    my_size_t rowsCheckpointsCovered, rowsSASampleCovered;
    rowsCheckpointsCovered = 1 + std::floor(this->parameterM * this->PARAMETER_B_RATIO);
    rowsSASampleCovered = 1 + std::floor(this->parameterM * this->PARAMETER_A_RATIO);

    this->occTally.resize(rowsCheckpointsCovered);
    this->SASamples.reserve(rowsSASampleCovered);

    // Call SA-IS on the source data --> while building the SA we will not keep all rows, only some!
    this->SA = this->SA_IS<BasePair>(*this->sourceData);
    this->constructedSA = true;

    this->constructBwt();
    this->SA.clear();           // Reduce memory usage
    this->SA.shrink_to_fit();

    // Let's establish the occTally
    this->occTally.resize(rowsCheckpointsCovered);
    for(auto& currRow : this->occTally) {
        currRow.fill(0);
    }

    // Let's fill in the occTally with values
    std::array<my_size_t, Util::ALPHABET_SIZE> countsTmp{};
    countsTmp.fill(0);

    for(my_size_t i = 0, chkptJ = 0; i < this->parameterM; i++) {
        BasePair currChar = this->bwt[i];
        ++(countsTmp[static_cast<my_size_t>(currChar)]);

        if(i % this->PARAMETER_B == 0) {
            // Checkpoint row
            auto& currRow = this->occTally[chkptJ++];
            currRow = countsTmp;
        }
    }

    this->constructedFMIndex = true;
}

void SA_IS_FMIndex::constructBwt() {
    if(!this->constructedSA) {
        throw logic_error("Cannot construct BWT until SA has been constructedFMIndex!");
    }

    // Init data structures here
    this->bwt.resize(this->parameterM);

    /*
     * 2 Rules that we have to follow for this algorithm:
     *      1. B[i] = '$' if SA[i] == 0
     *      2. B[i] = S[SA[i] - 1] in all other cases
     */
    my_size_t currNum;
    for(my_size_t i = 0; i < this->SA.size(); i++) {
        currNum = this->SA[i];
        if(currNum != 0) [[likely]] {
            this->bwt[i] = (*this->sourceData)[currNum - 1];
        } else [[unlikely]] {
            this->bwt[i] = BasePair::$;
        }

        if(i % PARAMETER_A == 0) {
            this->SASamples.emplace_back(currNum);
        }
    }

    this->constructedBwt = true;
}

template<typename T>
std::vector<my_size_t> SA_IS_FMIndex::SA_IS(const vector<T>& S) {
    /*
     * Okay - we have to do this by steps
     * It's a lengthy and painful algorithm
     * Take care to split step2 into functions so its substeps can be reused later in step 5
     */
    // Check if types are acceptable
    if(!is_same<T, BasePair>::value && !is_same<T, my_size_t>::value) {
        throw logic_error("SA_IS function should only be run with BasePair and my_size_t templates!");
    }

    // Step 0 - Prepare the data structures
    /*
     * The T array holds bits which tell which characters are S-Type and which characters are L-Type
     */
    std::vector<T_ARRAY_TYPE> tArray;

    /*
     * The P array holds the indices of LMS SubArrays.
     */
    std::vector<my_size_t> p1Array;

    /**
     * The P set is the same thing as the P array, except it's a map that maps pArr elements to their indices
     * The reason is because it enables an O(1) look-up operation, which is used in some of the SA-IS steps
     * We could use binary search on p1Array instead, but I would much prefer performance to memory
     */
    std::unordered_map<my_size_t, my_size_t> pMap;

    /*
     * The B Array basically consists of buckets for each letter(in alphabetical order).
     * After the algorithm takes its course this vector will become the SA (Suffix Array) from which
     * we will build the BWT(as well as keep some of its rows as occTally for the FM-Index).
     */
    std::vector<my_size_t> bArray;

    /*
     * These bucket trackers will be used for tracking buckets of individual characters in the bArray/SA
     */
    std::vector<BucketTracker> charBucketTrackers;

    /*
     * Counts will be necessary for both the FM-Index and the SA-IS algorithm
     * CountsCol is used in the first(BasePair) SA_IS iteration, while this countsMap is used in other iterations
     */
    std::unordered_map<my_size_t, my_size_t> countsMap;
    std::array<my_size_t, Util::ALPHABET_SIZE> counts{};

    bool isBasePairTemplate = is_same<T, BasePair>::value;

    // Prepare and reserve sizes
    tArray.resize(std::ceil((my_float_t)S.size() / SA_IS_FMIndex::T_ARRAY_TYPE_SIZE_BITS));
    std::fill(tArray.begin(), tArray.end(), 0);
    p1Array.reserve(std::ceil((my_float_t)S.size() / 2));
    pMap.reserve(p1Array.capacity());
    bArray.resize(S.size());

    if(isBasePairTemplate) {
        // First iterations
        countsMap.reserve(0);
        counts.fill(0);
        charBucketTrackers.resize(Util::ALPHABET_SIZE);
    } else {
        // Second+ iterations
        countsMap.reserve(S.size() - 1);            // It's S.size() - 1 because there will always be at least one repeated character else the function wouldn't have been called at all
    }

    // Step 1 - Form tArray and p1Array
    // $ is an S-type character
    ArrayBitIndices<SA_IS_FMIndex::T_ARRAY_TYPE> currTArrayPos{static_cast<my_size_t>(S.size() - 1), tArray, static_cast<my_size_t>(S.size())};
    tArraySetLetterType(currTArrayPos, LetterType::S_TYPE);

    T nextChar = S[S.size() - 1], currChar;
    LetterType nextCharType = LetterType::S_TYPE, currCharType;
    my_size_t tmpCastedVal;

    /**
     * LMS-SubArrays are formed from LMS-Characters(with the addition of containing the last index of the '$' sign)
     * LMS-Character is considered the second character in all "LS" substrings(the "S" character)
     * LMS-SubArrays are S[i,j] substrings where:
     *      both S[i] and S[j] are LMS-characters
     *      S[i, i] is the last, '$' character
     *
     * It is of note that, in case of recursive calls, last character/rank will always be 0, and represent the
     * '$' character and behave like i
     */
     if(isBasePairTemplate) {
         ++(counts[static_cast<BasePair_underlying_type>(BasePair::$)]);
     } else {
         countsMap.emplace(0, 1);
     }

    my_size_t pArrayIndex = 0;
    currTArrayPos.decrement();
    for(my_size_t i = S.size() - 2; i >= 0; --i) {
        // Move curr char pointer
        currChar = S[i];

        if(isBasePairTemplate) {
            ++(counts[static_cast<BasePair_underlying_type>(currChar)]);
        } else {
            tmpCastedVal = static_cast<my_size_t>(currChar);

            auto res = countsMap.emplace(tmpCastedVal, 1);
            if(!res.second) {
                // Insertion did not happen - increment the counter
                ++(res.first->second);
            }
        }

        /*
         * S-type: $ or s[i] < s[i+1] or s[i] == s[i+1] && s[i+1] is S-type
         * L-type: s[i] > s[i+1] or s[i] == s[i+1] && s[i+1] is L-type
         * Lexicographical order: L->S, L-types are lexicographically before S-types
         * Lexicographical order is enforced by the values of enum class representing S-Type and L-Type
         */
        if(currChar < nextChar) {
            // S-Type
            currCharType = LetterType::S_TYPE;
        } else if(currChar > nextChar) {
            // L-Type
            currCharType = LetterType::L_TYPE;
        } else {
            // They are equal, check whether the nextChar is S-Type or L-Type
            currCharType = nextCharType;
        }

        tArraySetLetterType(currTArrayPos, currCharType);
        currTArrayPos.decrement();

        // Check for LMS characters
        if(currCharType == LetterType::L_TYPE && nextCharType == LetterType::S_TYPE) {
            p1Array.emplace_back(i + 1);
            pMap.emplace(i+1, pArrayIndex);
            ++pArrayIndex;
        }

        // Move next pointers
        nextChar = currChar;
        nextCharType = currCharType;
    }

    // Form and prepare bArray and its corresponding data structures (buckets and pointers) --> We do this using the letter countsMap
    my_size_t cumulativeCount = 0;
    my_size_t currCount;
    if(isBasePairTemplate) {
        for(my_size_t i = 0; i < counts.size(); i++) {
            BucketTracker& currBucket = charBucketTrackers[i];
            currCount = counts[i];

            currBucket.setData(cumulativeCount, currCount);

            this->cumulativeCounts[i] = cumulativeCount;

            cumulativeCount += currCount;
        }
    } else {
        charBucketTrackers.resize(countsMap.size());
        my_size_t currentCharacter;

        // Must make sure here that its sorted and in ascending order
        for(currentCharacter = 0; currentCharacter < countsMap.size(); currentCharacter++) {
            currCount = countsMap[currentCharacter];
            BucketTracker& currBucket = charBucketTrackers[currentCharacter];

            currBucket.setData(cumulativeCount, currCount);

            cumulativeCount += currCount;
        }
    }

    countsMap.reserve(0);

    // Step 2 - Induced Sorting --> consists of 3 SubSteps
    /*
     * Step 2, SubStep 1 ->
     *      -> Set all SA members to -1
     *      -> Set every Bucket current pointer to the end of the corresponding Bucket
     *      -> Go through S left-to-right:
     *          -> Add indices of LMS-suffixes from S into corresponding SA Bucket end-to-start
     *          -> After each addition, move the Bucket current pointer one place to the left
     */
    std::fill(std::execution::par_unseq, bArray.begin(), bArray.end(), -1);

    std::for_each(std::execution::par_unseq, charBucketTrackers.begin(),
                  charBucketTrackers.end(),
                  [](BucketTracker& in){
                      in.resetToBack();
                  });

    BucketTracker* currBucketTracker;

    // TODO: left-to-right or right-to-left???
    for(my_size_t currIndex : ranges::reverse_view(p1Array)) {
        currChar = S[currIndex];

        if(isBasePairTemplate) {
            currBucketTracker = &charBucketTrackers[static_cast<BasePair_underlying_type>(currChar)];
        } else {
            currBucketTracker = &charBucketTrackers[static_cast<my_size_t>(currChar)];
        }

        bArray[currBucketTracker->current] = currIndex;
        currBucketTracker->decrement();
    }

    SA_IS_FMIndex::induceSortL(isBasePairTemplate, S, bArray, charBucketTrackers, currTArrayPos);
    SA_IS_FMIndex::induceSortS(isBasePairTemplate, S, bArray, charBucketTrackers, currTArrayPos);

    // Step 3 - Naming the LMS-SubArrays
    /*
     * To every LMS-SubString we have to assign a "name"(unique number) corresponding to its rank
     * If LMS LMS-SubStrings are equal --> of equal length, of equal letters and of equal letter types
     * at every position, then they will have same "names" assigned to them
     *
     * This new array will be known as S1
     *
     * 1. Go left-to-right through bArray(SA) generated by Step 2
     * 2. (in the SA all LMS-prefixes are now sorted)
     *      Check if the current suffix is also an LMS-SubString is done in O(1) time by checking if the starting character is an LMS-Character
     * 3. Are two LMS-SubStrings SA-neighbours:
     *      3a) They aren't --> give the LMS-SubString a new rank/name
     *      3b) They are --> compare them for equality:
     *          3b.1) If they are equal - give them the same name
     *          3b.2) If they are NOT equal - give them NEW names
     */
    vector<my_size_t> S1;
    S1.resize(p1Array.size());

    my_size_t nextRank;
    my_size_t tmpLMSEnd, tmpLMSStart;
    LMSSubString lmsPrev{}, lmsCurr{};
    bool lmsEqualTmp;
    LMSSubStringComparator lmsComp{S};
    my_size_t SAVal;

    bool duplicatesFound = false;

    /*
     * The element at SA position 0 is the '$' sign, which will always be a unique LMS-SubString
     * and therefore we give it a rank of 0. Also, it is located at the end of the P-Array since it is located at the
     * end of the S string, but when alphabetically sorted it lies at the start of the SA
     */
    nextRank = 0;
    S1[S1.size() - 1] = 0;
    lmsPrev.setData(S.size() - 1, S.size() - 1);        // Set previous LMS to be situated at the end of the string
    const my_size_t p1ArrayMaxIndex = (static_cast<my_size_t>(p1Array.size()) - 1);
    my_size_t p1MirroredIndex;

    for(my_size_t SAIndex = 1; SAIndex < bArray.size(); SAIndex++) {
        SAVal = bArray[SAIndex];
        auto res = pMap.find(SAVal);
        if(res != pMap.end()) {
            // This index is therefore an LMS-SubString, we need to get its index in the P1 array
            // Calculate rank according to the neighbourhood rule
            pArrayIndex = res->second;
            tmpLMSStart = p1Array[pArrayIndex];
            tmpLMSEnd = p1Array[pArrayIndex - 1];

            lmsCurr.setData(tmpLMSStart, tmpLMSEnd);

            // Keep the same rank or increase it
            // Check if they are the same!
            lmsEqualTmp = lmsComp(lmsPrev, lmsCurr);
            if(!lmsEqualTmp) {
                ++nextRank;
            } else {
                // Same rank means that duplicates have been found
                duplicatesFound = true;
            }

            // Update ranks
            p1MirroredIndex = p1ArrayMaxIndex - pArrayIndex;
            S1[p1MirroredIndex] = nextRank;

            // Update variables
            lmsPrev = lmsCurr;
        }
    }

    // Step 4 - If all the names are unique -> directly construct SA1 from S1,
    //  if not call this function recursively to construct SA1
    bArray.clear();
    bArray.shrink_to_fit();
    pMap.reserve(0);

    vector<my_size_t> SA1;
    if(duplicatesFound) {
        SA1 = this->SA_IS<my_size_t>(S1);
    } else {
        // Bucket sort S1 into SA1
        SA1.resize(S1.size());
        std::fill(std::execution::par_unseq, SA1.begin(), SA1.end(), -1);
        for(my_size_t i = 0; i < S1.size(); i++) {
            my_size_t currSA1Val = S1[i];
            SA1[currSA1Val] = i;
        }
    }

    S1.clear();
    S1.shrink_to_fit();
    bArray.resize(S.size());

    // Step 5 - Final SA determination from SA1
    /*
     * SubStep 1:
     *      -> Set all SA members to -1
     *      -> Set each Bucket current pointer to the end of the corresponding Bucket
     *      -> Iterate through SA1 right-to-left
     *          -> Put P1[SA1[i]] at the end of its corresponding Bucket, and move the Bucket current pointer one place to the left
     * SubStep 2 -> Same as Step 2:SubStep 2
     * SubStep 3 -> Same as Step 2:SubStep 3
     */
    std::fill(std::execution::par_unseq, bArray.begin(), bArray.end(), -1);
    std::for_each(std::execution::par_unseq, charBucketTrackers.begin(),
                  charBucketTrackers.end(),
                  [](BucketTracker& in){
                      in.resetToBack();
                  });

    my_size_t tmpP1Val;
    for(my_size_t SA1Val : ranges::reverse_view(SA1)) {
        p1MirroredIndex = p1ArrayMaxIndex - SA1Val;
        tmpP1Val = p1Array[p1MirroredIndex];

        // Now need to find this suffix and its Bucket
        currChar = S[tmpP1Val];

        if(isBasePairTemplate) {
            currBucketTracker = &charBucketTrackers[static_cast<BasePair_underlying_type>(currChar)];
        } else {
            currBucketTracker = &charBucketTrackers[static_cast<my_size_t>(currChar)];
        }

        bArray[currBucketTracker->current] = tmpP1Val;
        currBucketTracker->decrement();
    }

    SA1.clear();
    SA1.shrink_to_fit();

    SA_IS_FMIndex::induceSortL(isBasePairTemplate, S, bArray, charBucketTrackers, currTArrayPos);
    SA_IS_FMIndex::induceSortS(isBasePairTemplate, S, bArray, charBucketTrackers, currTArrayPos);

    // Return the SA
    return bArray;
}

template<typename T>
void SA_IS_FMIndex::induceSortL(bool isBasePairTemplate, const std::vector<T>& S, std::vector<my_size_t>& bArray,
                                std::vector<BucketTracker>& charBucketTrackers,
                                ArrayBitIndices<SA_IS_FMIndex::T_ARRAY_TYPE>& currTArrayPos) {
    /*
    * Step 2, SubStep 2 ->
    *      -> Set every Bucket current pointer to the start of every Bucket
    *      -> Go through SA left-to-right:
    *          -> If SA[i] > 0:
    *              -> k = S[SA[i] - 1]
    *              -> if k is L-Type: add SA[i] - 1 to the beginning of k-th Bucket
    *              -> After each addition move the Bucket current pointer one place to the right
    *              -> After each addition move the Bucket current pointer one place to the right
    */
    LetterType currLetterType;
    T currChar;
    BucketTracker* currBucketTracker;

    std::for_each(std::execution::par_unseq, charBucketTrackers.begin(),
                  charBucketTrackers.end(),
                  [](BucketTracker& in){
                      in.resetToFront();
                  });

    for(my_size_t SAVal : bArray) {
        if(SAVal > 0) {
            my_size_t leftSuffix = SAVal - 1;

            // Check if S[SAVal - 1] is an L-Type
            currTArrayPos.setIndex(leftSuffix);
            currLetterType = SA_IS_FMIndex::tArrayGetLetterType(currTArrayPos);
            if(currLetterType == LetterType::L_TYPE) {
                currChar = S[leftSuffix];

                if(isBasePairTemplate) {
                    currBucketTracker = &charBucketTrackers[static_cast<BasePair_underlying_type>(currChar)];
                } else {
                    currBucketTracker = &charBucketTrackers[static_cast<my_size_t>(currChar)];
                }

                bArray[currBucketTracker->current] = leftSuffix;
                currBucketTracker->increment();
            }
        }
    }
}

template<typename T>
void SA_IS_FMIndex::induceSortS(bool isBasePairTemplate, const vector<T>& S, std::vector<my_size_t>& bArray,
                                vector<BucketTracker>& charBucketTrackers,
                                ArrayBitIndices<T_ARRAY_TYPE>& currArrBitIndices) {
    /*
     * Step 2, SubStep 3 ->
     *      -> Set every Bucket current pointer to the end of the every Bucket
     *      -> Go through SA right-to-left:
     *          -> If SA[i] > 0:
     *              -> k = S[SA[i] - 1]
     *              -> If k is S-Type: add SA[i] -1 to the end of the k-th Bucket
     *              -> After each addition move the Bucket current pointer one place to the left
     */
    LetterType currLetterType;
    T currChar;
    BucketTracker* currBucketTracker;

    std::for_each(std::execution::par_unseq, charBucketTrackers.begin(),
                  charBucketTrackers.end(),
                  [](BucketTracker& in){
                      in.resetToBack();
                  });

    for(my_size_t SAVal : std::ranges::reverse_view(bArray)) {
        if(SAVal > 0) {
            my_size_t leftSuffix = SAVal - 1;

            // Check if S[leftSuffix] is an S-Type
            currArrBitIndices.setIndex(leftSuffix);
            currLetterType = tArrayGetLetterType(currArrBitIndices);
            if(currLetterType == LetterType::S_TYPE) {
                currChar = S[leftSuffix];

                if(isBasePairTemplate) {
                    currBucketTracker = &charBucketTrackers[static_cast<BasePair_underlying_type>(currChar)];
                } else {
                    currBucketTracker = &charBucketTrackers[static_cast<my_size_t>(currChar)];
                }

                bArray[currBucketTracker->current] = leftSuffix;
                currBucketTracker->decrement();
            }
        }
    }
}

std::string SA_IS_FMIndex::printTArray(vector<T_ARRAY_TYPE> &tArray, my_size_t elementsNum) {
    stringstream ss;
    ArrayBitIndices<T_ARRAY_TYPE> tArrayTracker{0, tArray, elementsNum};

    ss << "TArray:" << endl;

    for(my_size_t i = 0; i < elementsNum; i++) {
        LetterType currLetterType = SA_IS_FMIndex::tArrayGetLetterType(tArrayTracker);
        ss << (currLetterType == LetterType::S_TYPE ? 'S' : 'L');
        tArrayTracker.increment();
    }

    return ss.str();
}