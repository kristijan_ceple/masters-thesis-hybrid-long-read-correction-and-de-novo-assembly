//
// Created by kikyy_99 on 05.01.23..
//

#ifndef SW_PROJECT_MASTERS_THESIS_SA_IS_FMINDEX_H
#define SW_PROJECT_MASTERS_THESIS_SA_IS_FMINDEX_H


#include "Base_FMIndex.h"
#include <vector>
#include <cmath>
#include <map>
#include <limits>

class SA_IS_FMIndex : public Base_FMIndex {
private:
    using T_ARRAY_TYPE = uint8_t;

    class BucketTracker {
        friend class SA_IS_FMIndex;

        /**
         * Current iterator should be in range [start, end]
         * Therefore, start and end are INCLUSIVE
         */
        my_size_t start, end, current;

    public:
        BucketTracker() =default;

        inline BucketTracker(my_size_t beginIndex, my_size_t length) {
            this->setData(beginIndex, length);
        }

        inline void setData(my_size_t beginIndex, my_size_t length) {
            if(length > 0) [[likely]]{
                this->start = beginIndex;
                this->end = beginIndex + length - 1;
            } else[[unlikely]] {
                this->start = 0;
                this->end = 0;
            }

            this->current = this->end;
        }

        inline void resetToFront() {
            this->current = this->start;
        }

        inline void resetToBack() {
            this->current = this->end;
        }

        inline void increment() {
            ++(this->current);
        }

        inline void decrement() {
            --(this->current);
        }
    };

    enum class BitModifyOp: uint8_t {CLEAR = 0, SET = 1};
    enum class LetterType: uint8_t {L_TYPE = 0, S_TYPE = 1};

    /**
     * This class enables indexing individual bits in an array made up of larger data types(type T).
     * Used in conjuction with helper bit-Op functions this class enables getting, setting and clearing individual bits.
     *
     * Usually used for tracking S-Types and L-Types of characters in individual bits.
     *
     * @tparam T the type of data used to store bits --> also specifies the bit-width of the underlying type
     */
    template<typename T>
    class ArrayBitIndices {
        friend class SA_IS_FMIndex;

        /**
         * Most commonly trackedVector will be used for the tArray
         */
        std::vector<T>* trackedVector = nullptr;
        const my_size_t elementsTotalNum;

        my_size_t arrayElementIndex;
        uint16_t elementBitIndex;
        uint16_t arrayTypeSizeBits = sizeof(T) * 8;
        uint16_t arrayTypeLastElement = this->arrayTypeSizeBits - 1;

    public:
        explicit inline ArrayBitIndices(my_size_t index, std::vector<T>& trackedVector, my_size_t elementsTotalNum)
        : trackedVector{&trackedVector}, elementsTotalNum{elementsTotalNum} {
            this->setIndex(index);
        }

        inline ArrayBitIndices(my_size_t arrayElementIndex, uint16_t elementBitIndex, std::vector<T>& trackedVector, my_size_t elementsTotalNum)
        : trackedVector{&trackedVector}, elementsTotalNum{elementsTotalNum} {
            this->setIndex(arrayElementIndex, elementBitIndex);
        }

        inline void setIndex(my_size_t currIndex) {
            this->arrayElementIndex = currIndex / this->arrayTypeSizeBits;
            this->elementBitIndex = this->arrayTypeLastElement - static_cast<uint16_t>(currIndex % this->arrayTypeSizeBits);
        }

        inline void setIndex(my_size_t newArrayElementIndex, uint16_t newElementBitIndex) {
            this->arrayElementIndex = newArrayElementIndex;
            this->elementBitIndex = newElementBitIndex;
        }

        inline void increment() {
            if(this->elementBitIndex != 0) [[ likely ]]{
                --this->elementBitIndex;
            } else [[unlikely]] {
                this->elementBitIndex = this->arrayTypeLastElement;
                ++this->arrayElementIndex;
            }
        }

        inline void decrement() {
            if(this->elementBitIndex != this->arrayTypeLastElement) [[likely] ]{
                ++this->elementBitIndex;
            } else [[unlikely]]{
                this->elementBitIndex = 0;
                --this->arrayElementIndex;
            }
        }
    };

    /**
     * Used for comparing LMS SubStrings and keeping info about them
     * The LMS SubStrings are located in [start, end] --> the ranges are inclusive
     */
    class LMSSubString {
        friend class SA_IS_FMIndex;
        my_size_t start;
        my_size_t end;

        inline void setData(my_size_t newStart, my_size_t newEnd) {
            this->start = newStart;
            this->end = newEnd;
        }

        inline my_size_t compLength() const {
            return this->end - this->start;
        }
    };

    template<typename T>
    class LMSSubStringComparator {
        friend class SA_IS_FMIndex;
        const std::vector<T>& S;

        LMSSubStringComparator(const std::vector<T>& S) : S{S} {}

        inline bool operator()(const LMSSubString& firstLMS, const LMSSubString& secondLMS) {
            // First compare by length
            if(firstLMS.compLength() != secondLMS.compLength()) {
                return false;
            }

            // Then compare by characters
            T currChar1, currChar2;
            for(my_size_t i = firstLMS.start, j = secondLMS.start; i <= firstLMS.end; i++, j++) {
                currChar1 = S[i];
                currChar2 = S[j];
                if(currChar1 != currChar2) {
                    return false;
                }
            }

            return true;
        }
    };


    /*
     * Afterwards follow the SA_IS aspects of the algorithm
     */

    /**
     * One iteration of the SA-IS algorithm. Parameterised because the first iteration(that takes the entire S string)
     * uses a much smaller data type for each BasePair of the source data.
     *
     * The second(and further) iterations require much larger data counts(because of ranking)
     *
     * @tparam T The data width of the input data source
     * @param S The input data source of which SA1 will be constructedFMIndex
     * @return The SA1 (Suffix Array) constructedFMIndex from data source S
     */
    template<typename T>
    std::vector<my_size_t> SA_IS(const std::vector<T>& S);
    std::vector<my_size_t> SA;
    bool constructedSA = false;

    template<typename T>
    inline static void induceSortL(bool isBasePairTemplate, const std::vector<T>& S, std::vector<my_size_t>& bArray,
                                   std::vector<BucketTracker>& charBucketTrackers,
                                   ArrayBitIndices<SA_IS_FMIndex::T_ARRAY_TYPE>& currTArrayPos);

    template<typename T>
    inline static void induceSortS(bool isBasePairTemplate, const std::vector<T>& S, std::vector<my_size_t>& bArray,
                                   std::vector<BucketTracker>& charBucketTrackers,
                                   ArrayBitIndices<T_ARRAY_TYPE>& currArrBitIndices);

    void constructBwt() override;

    static std::string printTArray(std::vector<T_ARRAY_TYPE> &tArray, my_size_t elementsNum);
public:
    inline static constexpr uint16_t T_ARRAY_TYPE_SIZE_BITS = sizeof(SA_IS_FMIndex::T_ARRAY_TYPE) * Util::SIZEOF_BITS;

    inline static LetterType tArrayGetLetterType(const ArrayBitIndices<T_ARRAY_TYPE>& pos) {
        return LetterType{tArrayGetBitOp(pos)};
    }

    inline static constexpr void tArraySetLetterType(const ArrayBitIndices<T_ARRAY_TYPE>& pos, LetterType letterType) {
        switch(letterType) {
            case LetterType::S_TYPE:
                // S-type letters need their bits to be set to 1
                tArrayBitModifyOp(pos, BitModifyOp::SET);
                break;
            case LetterType::L_TYPE:
                // L-type letter need their bits to be cleared to 0
                tArrayBitModifyOp(pos, BitModifyOp::CLEAR);
                break;
            default:
                throw std::invalid_argument("Passed LetterType not supported!");
        }
    }

    inline static uint8_t tArrayGetBitOp(const ArrayBitIndices<T_ARRAY_TYPE>& pos) {
        uint8_t toRet;
        T_ARRAY_TYPE element = (*pos.trackedVector)[pos.arrayElementIndex];
        toRet = element>>(pos.elementBitIndex);
        toRet &= 0x1;
        return toRet;
    }

    inline static void tArrayBitModifyOp(const ArrayBitIndices<T_ARRAY_TYPE>& pos, BitModifyOp bitOp) {
        auto& element = (*pos.trackedVector)[pos.arrayElementIndex];

        uint8_t mask = 0x1 << pos.elementBitIndex;
        switch(bitOp) {
            case BitModifyOp::CLEAR:
                // Use AND operation to lower one bit at position bitIndex
                mask ^= 0xFF;
                element &= mask;
                break;
            case BitModifyOp::SET:
                // Use OR operation to raise one bit at position bitIndex;
                element |= mask;
                break;
            default:
                throw std::invalid_argument("Passed BitModifyOp not supported for T Array!");
        }
    }

    explicit inline SA_IS_FMIndex(std::vector<BasePair>* sourceData = nullptr,
            my_size_t parameterA = Base_FMIndex::DEFAULT_A_PARAM,
            my_size_t parameterB = Base_FMIndex::DEFAULT_B_PARAM,
            my_float_t suffixResultReservationFactor = Base_FMIndex::DEFAULT_SUFFIX_RESULT_RESERVATION_FACTOR)
            : Base_FMIndex{sourceData, parameterA, parameterB, suffixResultReservationFactor}
            {}

    void constructFMIndex() override;
};


#endif //SW_PROJECT_MASTERS_THESIS_SA_IS_FMINDEX_H