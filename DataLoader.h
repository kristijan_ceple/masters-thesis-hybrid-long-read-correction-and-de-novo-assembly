//
// Created by kikyy_99 on 20.01.23..
//

#ifndef SW_PROJECT_MASTERS_THESIS_DATALOADER_H
#define SW_PROJECT_MASTERS_THESIS_DATALOADER_H


#include <unordered_set>
#include <string>
#include <forward_list>
#include <memory>
#include <list>
#include "Util.h"
#include "SA_IS_FMIndex.h"

class DataLoader {
private:
    static std::string readInputBasicFastaFile(const std::string& filePath);
    static std::vector<BasePair> processInputString(std::string& in);

public:
    static inline const std::unordered_set<std::string> fastaExtensionsSet = {".fa", ".fasta" , ".fna" , ".ffn" ,  ".faa" , ".frn"};
    inline static constexpr char FASTA_COMMENT_CHAR = '>';
    inline static constexpr char FASTA_LINE_INFO_CHAR = '@';
    inline static constexpr my_size_t DEFAULT_BASES_UPPER_BOUND = 2E9;
    inline static void trimString(std::string& toTrim) {
        size_t start = toTrim.find_first_not_of(" \t\n\r");
        if(start == std::string_view::npos) {
            toTrim = "";
        }
        toTrim.erase(0, start);

        size_t end = toTrim.find_last_not_of(" \t\n\r");
        if(end == std::string_view::npos) {
            return;
        }
        toTrim.erase(end + 1);
    }

    static std::vector<BasePair> getInputData(const std::string& filePath);

    static std::vector<BasePair> getShortReadDataSet(const std::string& filePath, my_size_t basesUpperBound = DataLoader::DEFAULT_BASES_UPPER_BOUND);
    static std::pair<std::deque<std::vector<BasePair>>, my_size_t> getLongReadDataSet(const std::string& filePath);

    static void writeFMIndexToDisk(Base_FMIndex& fmIndex, const std::string& outputFilePath);
    static void writeLongReadDatasetToDisk(std::deque<std::vector<BasePair>>& longReadDataset, const std::string& outputFilePath);
    static void bwtRleEncode(const std::vector<BasePair>& bwt, std::ofstream& ofs);

    static std::unique_ptr<Base_FMIndex> loadFMIndexFromDisk(const std::string& inputFilePath);
    static std::vector<BasePair> bwtRleDecode(std::ifstream& bwtFile);
};




#endif //SW_PROJECT_MASTERS_THESIS_DATALOADER_H
