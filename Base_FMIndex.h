//
// Created by kikyy_99 on 15.12.22..
//

#ifndef SW_PROJECT_BASICSTRING_FMINDEX_H
#define SW_PROJECT_BASICSTRING_FMINDEX_H

#include <vector>
#include <stdexcept>
#include <array>
#include <deque>
#include "Util.h"

class Base_FMIndex {
    friend std::ostream& operator<<(std::ostream& os, const Base_FMIndex& obj);
    friend class DataLoader;

protected:
    /**
     * Also called the 'L' or 'B' column
     */
    std::vector<BasePair> bwt;

    /**
     * Function/Array c: amount of characters that lexicographically precede
     * the current character
     *
     * Also called the F column, or the c BW_Count function, or the cumulativeCounts column
     */
    std::array<my_size_t, Util::ALPHABET_SIZE> cumulativeCounts;
    std::vector<my_size_t> SASamples;
    void fillWithSASamples(std::deque<my_size_t>& toFill, my_size_t leftBound, my_size_t rightBound);

    /**
     * For now - occTally counts the num of characters in BWT[0:i], range inclusive
     * Which means that it contains the number of occurences, not B-sort indices
     *
     * Each sub-element(array) is basically a row, consisting of the count
     * of each alphabet character
     *
     * Keep in mind that occTally actually consists of checkpoints, so rows cannot be directly queried
     */
    std::vector<std::array<my_size_t, Util::ALPHABET_SIZE>> occTally;
    my_size_t occ(BasePair currBp, BasePair_underlying_type currBpInt, my_size_t bound);

    void checkCharAndIncrementCounter(BasePair currBp, my_size_t bwtIndex, my_size_t& counter);
    void checkCharAndDecrementCounter(BasePair currBp, my_size_t bwtIndex, my_size_t& counter);
    my_size_t LFMapping(BasePair bwtBp, my_size_t bwtIndex);

    std::vector<BasePair>* sourceData;
    /**
     * Parameter M is basically the size of the input data
     * Includes the terminating '$' character
     */
    my_size_t parameterM;
    /**
     * Parameter A tells how many rows one SA Sample covers
     */
    my_size_t PARAMETER_A;
    /**
     * Parameter B tells how many rows of data one Checkpoint covers
     */
    my_size_t PARAMETER_B;
    my_size_t PARAMETER_B_HALF;
    my_float_t PARAMETER_B_RATIO;

    my_float_t PARAMETER_A_RATIO;

    /**
     * Allocate in advance the vector containing FM Index query results.
     * This parameter divides the (bwt.size() - queryStr.size()) number.
     */
    my_float_t suffixResultReservationFactor;
    bool constructedFMIndex = false;
    bool constructedBwt = false;

    template<typename T>
    inline std::pair<my_size_t, my_size_t> BWCount(T suffixToCheckIt, my_size_t len) {
        my_size_t i = len - 1;
        BasePair currChar = suffixToCheckIt[i];
        auto currCharInt = static_cast<BasePair_underlying_type>(currChar);

        my_size_t leftBound, rightBound;
        leftBound = this->cumulativeCounts[currCharInt];

        if(currCharInt == Util::ALPHABET_SIZE-1) [[unlikely]] {
            rightBound = this->parameterM - 1;
        } else [[likely]] {
            rightBound = this->cumulativeCounts[currCharInt+1] - 1;
        }

        while((leftBound <= rightBound) && (i >= 1)) {
            --i;
            currChar = suffixToCheckIt[i];
            currCharInt = static_cast<BasePair_underlying_type>(currChar);

            leftBound = this->cumulativeCounts[currCharInt] + this->occ(currChar, currCharInt, leftBound - 1);
            rightBound = this->cumulativeCounts[currCharInt] + this->occ(currChar, currCharInt, rightBound) - 1;
        }

        return std::make_pair(leftBound, rightBound);
    };

public:
    inline static constexpr my_size_t DEFAULT_A_PARAM = 32;
    inline static constexpr my_size_t DEFAULT_B_PARAM = 128;
    inline static constexpr my_float_t DEFAULT_SUFFIX_RESULT_RESERVATION_FACTOR = 1.0;

    /**
     * Constructs single-string FM Index
     * Note: source string shouldn't have a '$' sign
     *
     * @param sourceStr
     * @param parameterA Number of rows that 1 checkpoint will cover
     * @param parameterB Number of rows that one SA Sample will cover
     */
    explicit inline Base_FMIndex(std::vector<BasePair>* sourceStr,
                                 my_size_t parameterA = Base_FMIndex::DEFAULT_A_PARAM,
                                 my_size_t parameterB = Base_FMIndex::DEFAULT_B_PARAM,
                                 my_float_t suffixResultReservationFactor = Base_FMIndex::DEFAULT_SUFFIX_RESULT_RESERVATION_FACTOR)
        : sourceData{sourceStr}, PARAMETER_A{parameterA}, PARAMETER_B{parameterB}, PARAMETER_B_HALF{(parameterB - 1) / 2},
          PARAMETER_A_RATIO{1. / static_cast<double>(parameterA)}, PARAMETER_B_RATIO{1. / static_cast<double>(parameterB)},
          parameterM{static_cast<my_size_t>(sourceStr->size()) + 1}, suffixResultReservationFactor{suffixResultReservationFactor} {
        // Append the dollar as we prepare for the algo
        this->sourceData->emplace_back(BasePair::$);
    };
    Base_FMIndex() =default;

    virtual void constructFMIndex();
    virtual void constructBwt();
    virtual std::deque<my_size_t> query(std::vector<BasePair>::const_iterator suffixToCheckIt, my_size_t len);

    virtual inline my_size_t queryNum(std::vector<BasePair>::const_iterator suffixToCheckIt, my_size_t len) {
//        std::cout << "Querying: ";
//        for(my_size_t i = 0; i < len; i++) {
//            std::cout << Util::basePair2Char(suffixToCheckIt[i]);
//        }
//        std::cout << std::endl;

        auto [leftBound, rightBound] = this->BWCount(suffixToCheckIt, len);
        return (rightBound - leftBound + 1);
    }

    virtual inline my_size_t queryNum(std::deque<BasePair>::const_iterator suffixToCheckIt, my_size_t len) {
//        std::cout << "Querying: ";
//        for(my_size_t i = 0; i < len; i++) {
//            std::cout << Util::basePair2Char(suffixToCheckIt[i]);
//        }
//        std::cout << std::endl;

        auto [leftBound, rightBound] = this->BWCount(suffixToCheckIt, len);
        return (rightBound - leftBound + 1);
    }

    /*
     * GETTERS
     */
    inline bool isConstructedFMIndex() const { return this->constructedFMIndex; };

    inline const std::vector<BasePair>& getBwt() {
        if(this->constructedBwt) {
            return this->bwt;
        } else {
            throw std::logic_error("Trying to retrieve FM Index data structures but the conversion hasn't been performed yet");
        }
    };

    inline const std::array<my_size_t, Util::ALPHABET_SIZE>& getCumulativeCountsCol() {
        if(this->constructedFMIndex) {
            return this->cumulativeCounts;
        } else {
            throw std::logic_error("Trying to retrieve FM Index data structures but the conversion hasn't been performed yet");
        }
    }
};


#endif //SW_PROJECT_BASICSTRING_FMINDEX_H
