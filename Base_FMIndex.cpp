//
// Created by kikyy_99 on 15.12.22..
//

#include "Base_FMIndex.h"
using namespace std;

std::ostream& operator<<(std::ostream& os, const vector<BasePair>& obj);
std::ostream& operator<<(std::ostream& os, const vector<my_size_t>& obj);

std::ostream& operator<<(std::ostream& os, const Base_FMIndex& obj) {
    if(obj.sourceData != nullptr) {
        os << "Input String:" << endl << *obj.sourceData << endl;
    }

    if(obj.constructedBwt) {
        os << endl << "BWT:" << endl << '[';
        os << obj.bwt;
        os << ']';
    }

    return os;
}

my_size_t Base_FMIndex::occ(BasePair currBp, BasePair_underlying_type currBpInt, my_size_t bound) {
    /*
     * This function finds and returns the number of character occurences in
     * the bwt/L column in range B[0,bound]
     *
     * In order to do that, we have to take care of checkpoints and their spacing
     * 1. Find the closest checkpoint
     * 2. Increment/decrement number of chars on the way to it
     */
    // First find the closest checkpoint
    my_size_t relativePos = bound % this->PARAMETER_B;
    my_size_t closestChkptIndex = bound * this->PARAMETER_B_RATIO;
    bool goDown = false;

    if((relativePos > this->PARAMETER_B_HALF) && (closestChkptIndex + 1 < this->occTally.size())) {
        ++closestChkptIndex;
        goDown = true;
    }

    // Let's get the num of the selected char
    my_size_t currBpNum = this->occTally[closestChkptIndex][currBpInt];
    my_size_t bwtStartIndex = closestChkptIndex * this->PARAMETER_B;
    if(goDown) {
        for(my_size_t i = bwtStartIndex; i > bound; --i) {
//            checkCharAndDecrementCounter(currBp, i, currBpNum);
            if(this->bwt[i] == currBp) {
                --currBpNum;
            }
        }
    } else {
        for(my_size_t i = bwtStartIndex+1; i <= bound; ++i) {
//            checkCharAndIncrementCounter(currBp, i, currBpNum);
            if(this->bwt[i] == currBp) {
                ++currBpNum;
            }
        }
    }

    return currBpNum;
}

inline void Base_FMIndex::checkCharAndIncrementCounter(BasePair currBp, my_size_t bwtIndex, my_size_t& counter) {
    if(this->bwt[bwtIndex] == currBp) {
        ++counter;
    }
}

inline void Base_FMIndex::checkCharAndDecrementCounter(BasePair currBp, my_size_t bwtIndex, my_size_t& counter) {
    if(this->bwt[bwtIndex] == currBp) {
        --counter;
    }
}

void Base_FMIndex::fillWithSASamples(deque<my_size_t> &toFill, my_size_t leftBound, my_size_t rightBound) {
    /*
     * We have to go [leftBound, rightBound], find for each one of them
     * their SA values(indices at which they are located in the source data basically)
     *
     * We will have to do that using SA, but more specifically -> using SA Samples
     */
    my_size_t SASampleIndex;
    my_size_t SASampleRemainder;
    BasePair bwtChar;
    my_size_t stepsMade;
    my_size_t currRowIndex;
    for(my_size_t i = leftBound; i <= rightBound; i++) {
        // Check if the SA Sample contains the sample for the current row
        currRowIndex = i;
        SASampleIndex = currRowIndex * this->PARAMETER_A_RATIO;
        SASampleRemainder = currRowIndex % this->PARAMETER_A;
        stepsMade = 0;

        while(SASampleRemainder != 0) {
            ++stepsMade;
            bwtChar = this->bwt[currRowIndex];
            currRowIndex = this->LFMapping(bwtChar, currRowIndex);

            SASampleIndex = currRowIndex * this->PARAMETER_A_RATIO;
            SASampleRemainder = currRowIndex % this->PARAMETER_A;
        }

        toFill.emplace_back((this->SASamples[SASampleIndex] + stepsMade) % this->parameterM);
    }
}

/**
 * Find and return i-th occurence of the F-value for this L-Value
 * In simple words: find this BWT character in the F column
 *
 * @param bwtBp current BWT character
 * @param bwtIndex the index of the BWT character
 * @return index of this character in the F column
 */
inline my_size_t Base_FMIndex::LFMapping(BasePair bwtBp, my_size_t bwtIndex) {
    return this->occ(bwtBp, static_cast<BasePair_underlying_type>(bwtBp), bwtIndex) + this->cumulativeCounts[static_cast<BasePair_underlying_type>(bwtBp)] - 1;
}

void Base_FMIndex::constructFMIndex() {
    // TODO: Can fill this later with rotations
}

void Base_FMIndex::constructBwt() {
    // TODO: Can fill this later with rotations
}

std::deque<my_size_t> Base_FMIndex::query(std::vector<BasePair>::const_iterator suffixToCheckIt, my_size_t len) {
    deque<my_size_t> res;

    auto [leftBound, rightBound] = this->BWCount(suffixToCheckIt, len);
    this->fillWithSASamples(res, leftBound, rightBound);

    return res;
}