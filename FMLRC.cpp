//
// Created by kikyy_99 on 20.01.23..
//

#include <syncstream>
#include <iostream>
#include <fmt/format.h>
#include <deque>
#include <set>
#include <limits>
#include "FMLRC.h"
using namespace std;

std::ostream& operator<<(std::ostream& os, const vector<my_float_t>& obj);

void FMLRC::execute() {
    this->distributeJobsToThreads();

    for(auto& currThread : this->threads) {
        currThread.join();
    }

    // Print averages
    if(this->QUALITY_ASSURANCE) {
        cout << "Averages before:" << endl;
        cout << this->averageLongReadFrequenciesBefore << endl;

        cout << "Averages after:" << endl;
        cout << this->averageLongReadFrequenciesAfter << endl;
    }

    this->resultReady = true;
}

void FMLRC::distributeJobsToThreads() {
    my_size_t jobsPerWorker = this->longReadDatasetSize / this->threadsNum;
    my_size_t remainingJobs = this->longReadDatasetSize % this->threadsNum;

    my_size_t jobsToDo, beginJobIndex = 0;

    for(my_size_t i = 0; i < this->threadsNum; i++) {
        jobsToDo = jobsPerWorker;

        if(remainingJobs > 0) {
            ++jobsToDo;
            --remainingJobs;
        }

        this->threads.emplace_back(FMLRC::threadWorker, i, beginJobIndex, jobsToDo, this);
        beginJobIndex += jobsToDo;
    }
}

void FMLRC::threadWorker(my_size_t myID, my_size_t beginJobIndex, my_size_t jobsToDo, FMLRC* myObj) {
//    osyncstream(cout) << fmt::format("Thread ID: {} working on range [{}, {}]", myID, beginJobIndex, (beginJobIndex + jobsToDo - 1)) << endl;

    for(my_size_t i = 0; i < jobsToDo; ++i) {
//        osyncstream(cout) << fmt::format("Thread ID: {} currently working on Job ID: {}", myID, (beginJobIndex + i)) << endl;

        vector<BasePair>& currLongRead = (*myObj->longReadDataset)[beginJobIndex + i];

        // Calculate average k-mer frequencies before correction
        // Short k-mer pass
        auto little_k_res = myObj->onePass(currLongRead, myObj->PARAMETER_k, beginJobIndex+i, myObj->QUALITY_ASSURANCE);

        // Perform corrections
        myObj->performCorrections(little_k_res, currLongRead);

        // Long K-mer pass
        if(myObj->longKPass) {
            auto big_k_res = myObj->onePass(currLongRead, myObj->PARAMETER_K, beginJobIndex + i, false);

            // Perform correction
            myObj->performCorrections(big_k_res, currLongRead);
        }

        // Calculate average kmer frequencies after correction
        myObj->calculateAverage(currLongRead, beginJobIndex + i, myObj->averageLongReadFrequenciesAfter, myObj->PARAMETER_k);
    }

}

std::forward_list<FMLRC::SingleCorrection> FMLRC::onePass(vector<BasePair>& currLongRead, my_size_t kmerLen, my_size_t myIndex, bool calcPreAvg) {
    if(currLongRead.empty()) {
        return {};
    }

    std::forward_list<SingleCorrection> thisPassCorrections{};

    // Calculate branch limit -> L = B * k
    my_size_t branchLimit = this->PARAMETER_B * kmerLen;

    // Get reverse-complemented sequence of current long read
    auto currLongReadRevComp = std::move(std::get<vector<BasePair>>(FMLRC::reverseAndComplement(currLongRead.cbegin(), currLongRead.size())));

    // Get all k-mers counts in current long read
    auto resPair = this->allKmerFrequencies(currLongRead, kmerLen, &currLongReadRevComp, true, this->PARAMETER_T);

    // Calculate the average if we're doing the little k run
    vector<my_size_t>& kmerCounts = resPair.first;

    if(calcPreAvg) {
        this->calculateAverage(currLongRead, myIndex, this->averageLongReadFrequenciesBefore, kmerLen, &kmerCounts);
    }

    // Calculate and check median
    multiset<my_size_t>& pKmerSortedCounts = resPair.second;

    my_float_t currLongReadMedian = FMLRC::calculateMedianFreq(pKmerSortedCounts);
    if(currLongReadMedian < this->PARAMETER_T) {
        return {};
    }

    my_size_t currThreshold = std::max(this->PARAMETER_T, static_cast<my_size_t>(this->PARAMETER_F * currLongReadMedian));

    my_size_t firstAnchorIndex, secondAnchorIndex;
    my_size_t maxBranchLength;
    KmerType currKmerType;
    my_size_t tmp;

    /*
     * Algorithm begins here
     */
    my_size_t currKmerIndex = 0;
    my_size_t currKmerCount = kmerCounts[0];

    // First we do the head case(if possible) --> seed-and-extend
//    currKmerType = FMLRC::kmerType(currKmerCount, currThreshold);
    currKmerType = currKmerCount < currThreshold ? KmerType::WEAK : KmerType::SOLID;

    if(currKmerType == KmerType::WEAK) {
        // Find the next solid anchor
        firstAnchorIndex = FMLRC::findTheNextKmerType(kmerCounts, currKmerIndex, currThreshold, KmerType::SOLID);
        if(firstAnchorIndex == kmerCounts.size()) {
            // This whole long read is one massive weak region - nothing we can do
            return {};
        }

        // Seed-and-extend
        maxBranchLength = this->PERIPHERY_BRIDGE_BUFFER_FACTOR * (firstAnchorIndex + kmerLen);
        if(maxBranchLength <= this->MAX_GAP_LENGTH) {
            auto bestBridge = this->seedAndExtendHead(currLongRead.begin(),
                                                      currLongRead.begin() + firstAnchorIndex, firstAnchorIndex,
                                                      kmerLen, branchLimit, maxBranchLength, currThreshold);
            if(!bestBridge.empty()) {
                // Mark this correction
                thisPassCorrections.emplace_front(0, firstAnchorIndex + kmerLen, std::move(bestBridge));
            }
        }
    } else {
        firstAnchorIndex = 0;
    }

    // Now we will attempt to bridge all the weak regions --> multiple seed-and-bridge
    // Or, if the long read is too small - proceed to the tail case
    currKmerIndex = firstAnchorIndex;

    // Middle part
    while(currKmerIndex < kmerCounts.size()) {          // Guardian loop
        // We need to find solid - weak - solid region case
        // Place two anchors around the weak region, and then bridge it

        // Find the next weak region
        tmp = FMLRC::findTheNextKmerType(kmerCounts, currKmerIndex, currThreshold, KmerType::WEAK);
        if(tmp == kmerCounts.size()) [[unlikely]] {
            // No more weak regions? Then we've hit a wall, time to return the corrections
            return thisPassCorrections;
        }

        firstAnchorIndex = tmp - 1;

        // Find the next solid region
        tmp = FMLRC::findTheNextKmerType(kmerCounts, tmp, currThreshold, KmerType::SOLID);
        if(tmp == kmerCounts.size()) [[unlikely]] {
            break;      // Go to tail case
        }

        secondAnchorIndex = tmp;

        // Have both anchors - seed-and-bridge between them
        maxBranchLength = this->MID_BRIDGE_BUFFER_FACTOR * (secondAnchorIndex - firstAnchorIndex + kmerLen);
        if(maxBranchLength <= this->MAX_GAP_LENGTH) {
            auto bestBridges = this->seedAndBridge(currLongRead.begin() + firstAnchorIndex, currLongRead.begin() + secondAnchorIndex, kmerLen, branchLimit, maxBranchLength, currThreshold);
            if(!bestBridges.empty()) {
                // Mark this correction
                if(bestBridges.size() == 2) {
                    // Midpoint case
                    auto& firstHalf = bestBridges[0];
                    auto& secondHalf = bestBridges[1];
                    my_size_t midPointIndex = (firstAnchorIndex + secondAnchorIndex + kmerLen) * 0.5;

                    thisPassCorrections.emplace_front(firstAnchorIndex, firstHalf.size(), std::move(firstHalf));
                    thisPassCorrections.emplace_front(midPointIndex, secondHalf.size(), std::move(secondHalf));
                } else {
                    // Normal case, thank the Gods
                    auto& toCorr = bestBridges[0];
                    thisPassCorrections.emplace_front(firstAnchorIndex, toCorr.size(), std::move(bestBridges[0]));
                }
                bestBridges.clear();
                bestBridges.shrink_to_fit();
            }
        } else {
            // Cannot bridge this region - move on to the next weak region
        }

        currKmerIndex = secondAnchorIndex;
    }

    // Last, we do the tail case(if possible) --> seed-and-extend
    // Seed-and-extend
    currKmerIndex = kmerCounts.size();
    maxBranchLength = this->PERIPHERY_BRIDGE_BUFFER_FACTOR * (currKmerIndex - firstAnchorIndex + kmerLen);
    if(maxBranchLength <= this->MAX_GAP_LENGTH) {
        my_size_t weakRegionSize = (currKmerIndex - firstAnchorIndex);
        auto bestBridge = this->seedAndExtendTail(currLongRead.begin() + firstAnchorIndex, firstAnchorIndex, kmerLen,
                                                  branchLimit, maxBranchLength, currThreshold, weakRegionSize);
        if(!bestBridge.empty()) {
            // Mark this correction
            thisPassCorrections.emplace_front(firstAnchorIndex, weakRegionSize, std::move(bestBridge));
        }
    }

    return thisPassCorrections;
}

std::deque<BasePair>
FMLRC::seedAndExtendHead(std::vector<BasePair>::iterator regionBeginIt, std::vector<BasePair>::iterator anchorKmerIt,
                         my_size_t anchorIndex, my_size_t kmerLen, my_size_t branchLimit, my_size_t maxBranchLength,
                         my_size_t currThreshold) {
    auto anchorComplement = std::move(std::get<vector<BasePair>>(FMLRC::reverseAndComplement<std::vector<BasePair>::iterator>(anchorKmerIt, kmerLen)));

    // Short assemble here
    auto bestBridges = this->findBridges(anchorComplement.begin(), kmerLen, branchLimit, maxBranchLength, currThreshold, anchorKmerIt, false);
    if(bestBridges.empty()) {
        return {};
    }

    // Complement this region to use it in the shortest edit distance algorithm - range [0, firstAnchor + kmerLen>
    auto regionComplement = std::move(std::get<std::vector<BasePair>>(FMLRC::reverseAndComplement(regionBeginIt, anchorIndex + kmerLen)));

    // Let's find the bridge with the shortest edit distance
    auto toRetBestBridge = std::move(this->minEditDistanceBridge(bestBridges, regionComplement.begin(), regionComplement.size(), kmerLen).second);
    bestBridges.clear();
    bestBridges.shrink_to_fit();

    // Just reverse complement it
    auto toRetComplBestBridge = std::get<deque<BasePair>>(FMLRC::reverseAndComplement<deque<BasePair>>(toRetBestBridge));

    // Cull the size to fit
    my_size_t targetSize = anchorIndex + kmerLen;
    if(toRetComplBestBridge.size() > targetSize) {
        toRetComplBestBridge.resize(targetSize);
    }

    return toRetComplBestBridge;
}

std::vector<std::deque<BasePair>> FMLRC::seedAndBridge(std::vector<BasePair>::iterator firstAnchorKmerIt, std::vector<BasePair>::iterator secondAnchorKmerIt, my_size_t kmerLen, my_size_t branchLimit, my_size_t maxBranchLength, my_size_t currThreshold) {
    std::vector<std::deque<BasePair>> toRet;

    // Using non-reverse complemented anchors = normal anchors
    auto bestBridges = this->findBridges(firstAnchorKmerIt, kmerLen, branchLimit, maxBranchLength, currThreshold, secondAnchorKmerIt, true);
    if(bestBridges.empty()) {
        // Try reverse complement!
        auto firstAnchorComplement = std::move(std::get<vector<BasePair>>(FMLRC::reverseAndComplement<std::vector<BasePair>::iterator>(firstAnchorKmerIt, kmerLen)));
        auto secondAnchorComplement = std::move(std::get<vector<BasePair>>(FMLRC::reverseAndComplement<std::vector<BasePair>::iterator>(secondAnchorKmerIt, kmerLen)));

        bestBridges = this->findBridges(secondAnchorComplement.begin(), kmerLen, branchLimit, maxBranchLength, currThreshold, firstAnchorComplement.begin(), true);
        if(!bestBridges.empty()) {
            // Return the results back to normal(non-reverse complemented)
            for(deque<BasePair>& currBestBridge : bestBridges) {
                currBestBridge = std::move(std::get<std::deque<BasePair>>(FMLRC::reverseAndComplement<deque<BasePair>>(currBestBridge)));
            }

            // For non-midpoint algorithm the procedure is pretty straightforward
            // Find the minimum edit distance bridge and note it down
            my_size_t tmpLen = std::distance(firstAnchorKmerIt, secondAnchorKmerIt) + kmerLen;
            toRet.emplace_back(std::move(this->minEditDistanceBridge(bestBridges, firstAnchorKmerIt, tmpLen, kmerLen).second));
            bestBridges.clear();
            bestBridges.shrink_to_fit();

            // Cull if too large
            if(toRet[0].size() > tmpLen) {
                toRet[0].resize(tmpLen);
            }
        } else {
            // Get a midpoint, and then seed-and-extend from both sides to the middle
            my_size_t weakRegionSize = std::distance(firstAnchorKmerIt, secondAnchorKmerIt);
            my_size_t midPointOffset = (weakRegionSize + kmerLen) * 0.5;
            maxBranchLength = midPointOffset * this->PERIPHERY_BRIDGE_BUFFER_FACTOR;

            if(maxBranchLength <= this->MAX_GAP_LENGTH) {
                // First try to bridge from left to midpoint
                // Short assemble here
                bestBridges = this->findBridges(firstAnchorKmerIt, kmerLen, branchLimit, maxBranchLength, currThreshold, secondAnchorKmerIt, false);
                if(!bestBridges.empty()) {
                    // Let's find the bridge with the shortest edit distance
                    auto tmpMinEditRes = this->minEditDistanceBridge(bestBridges, firstAnchorKmerIt, midPointOffset, kmerLen);
                    my_size_t minEditDist = tmpMinEditRes.first;

                    if(minEditDist <= midPointOffset * this->MID_POINT_BRIDGE_EDIT_DIST_FACTOR) {
                        // Mark this correction down

                        // Cull if too large
                        if(tmpMinEditRes.second.size() > midPointOffset) {
                            tmpMinEditRes.second.resize(midPointOffset);
                        }

                        toRet.emplace_back(std::move(tmpMinEditRes.second));
                    }

                    bestBridges.clear();
                    bestBridges.shrink_to_fit();
                }

                // Then try to bridge from right to midpoint
                bestBridges = this->findBridges(secondAnchorComplement.begin(), kmerLen, branchLimit, maxBranchLength, currThreshold, secondAnchorKmerIt, false);
                if(!bestBridges.empty()) {
                    // Let's find the bridge with the shortest edit distance
                    // Need to complement the sequence in [midPoint, secondAnchor + kmerLen>
                    my_size_t tmpLen = std::distance(firstAnchorKmerIt + midPointOffset, secondAnchorKmerIt + kmerLen);
                    auto rightHalfComplement = std::move(std::get<vector<BasePair>>(FMLRC::reverseAndComplement<std::vector<BasePair>::iterator>(firstAnchorKmerIt + midPointOffset, tmpLen)));

                    auto tmpMinEditRes = this->minEditDistanceBridge(bestBridges, rightHalfComplement.begin(), tmpLen, kmerLen);
                    my_size_t minEditDist = tmpMinEditRes.first;

                    if(minEditDist <= midPointOffset * this->MID_POINT_BRIDGE_EDIT_DIST_FACTOR) {
                        // Mark this correction down
                        // Get the complement of the best bridge
                        auto tmpMinBridge = std::move(tmpMinEditRes.second);
                        auto tmpComplementedMinBridge = std::move(std::get<deque<BasePair>>(FMLRC::reverseAndComplement<std::deque<BasePair>>(tmpMinBridge)));

                        // Cull if too large
                        if(tmpComplementedMinBridge.size() > tmpLen) {
                            tmpComplementedMinBridge.resize(tmpLen);
                        }

                        toRet.emplace_back(std::move(tmpComplementedMinBridge));
                    }

                    bestBridges.clear();
                    bestBridges.shrink_to_fit();
                }
            }
        }
    }

    return toRet;
}

std::deque<std::deque<BasePair>> FMLRC::findBridges(std::vector<BasePair>::iterator anchorKmer, my_size_t kmerLen, my_size_t branchLimit, my_size_t maxBranchLength, my_size_t currThreshold, std::vector<BasePair>::iterator secondAnchorKmerIt, bool secondAnchor) {
    my_size_t currBridgeLen;
    my_size_t branchesCnt = 0;
    std::deque<std::deque<BasePair>> toRet;

    std::deque<std::deque<BasePair>> bridgesQueue;
    bridgesQueue.emplace_back(anchorKmer, anchorKmer + kmerLen);

    std::array<my_size_t, Util::ALPHABET_SIZE> bpFreqsTracker{};
    bpFreqsTracker[0] = 0;
    BasePair maxCountBp;
    BasePair maxCountBpCompl;
    my_size_t maxCount;

    currBridgeLen = kmerLen;

    std::deque<BasePair> movingWindow;
    std::deque<BasePair> movingWindowRevComp;

    while(!bridgesQueue.empty()) {
        if(branchesCnt > branchLimit) {
            return {};
        }

        ++branchesCnt;

        std::deque<BasePair>& currBridge = bridgesQueue.front();
        currBridgeLen = currBridge.size();

        auto currBridgeRevComp = std::move(std::get<deque<BasePair>>(FMLRC::reverseAndComplement<std::deque<BasePair>>(currBridge)));

        // Prepare the moving window first!
        for(my_size_t i = 0, j = currBridgeLen - kmerLen; i < kmerLen; ++i, ++j) {
            BasePair currBp = currBridge[j];
            movingWindow.emplace_back(currBp);
            movingWindowRevComp.emplace_front(Util::basePair2ComplementBasePair(currBp));
        }

        // Time for bridge extension
        while(currBridgeLen < maxBranchLength) {
            movingWindow.pop_front();
            movingWindowRevComp.pop_back();

            maxCountBp = BasePair::$;
            maxCount = 0;

            // Skip testing the dollar, that's why we go from 1 and not 0(because $'s value is 0)
//            {
//                std::lock_guard<std::mutex> lg{this->fmIndexMutex};
                for(BasePair_underlying_type i = 1; i < Util::ALPHABET_SIZE; ++i) {
                    auto currBp = static_cast<BasePair>(i);
                    movingWindow.emplace_back(currBp);
                    movingWindowRevComp.emplace_front(Util::basePair2ComplementBasePair(currBp));

                    bpFreqsTracker[i] = this->fmIndex->queryNum(movingWindow.begin(), movingWindow.size());
                    bpFreqsTracker[i] += this->fmIndex->queryNum(movingWindowRevComp.begin(), movingWindowRevComp.size());

                    if(bpFreqsTracker[i] > maxCount) {
                        maxCount = bpFreqsTracker[i];
                        maxCountBp = currBp;
                    }

                    movingWindowRevComp.pop_front();
                    movingWindow.pop_back();
                }
//            }

            // Check if best form bridge is good enough!
            if(maxCount < currThreshold) {
                // The best bridge isn't good enough, no luck here, check other bridges
                break;
            } else {
                // Maybe others can be useful candidates as well?
                // Skip testing the dollar, that's why we go from 1 and not 0(because $'s value is 0)
                for(BasePair_underlying_type i = 1; i < Util::ALPHABET_SIZE; i++) {
                    if(i != static_cast<BasePair_underlying_type>(maxCountBp)
                    && bpFreqsTracker[i] >= currThreshold) {
                        // Another possible candidate! Add it to the queue so it can be processed
                        currBridge.emplace_back(static_cast<BasePair>(i));
                        bridgesQueue.emplace_back(currBridge);
                        currBridge.pop_back();
                    }
                }

                // Great! We can extend our bridge, and then attempt to extend it even further!
                maxCountBpCompl = Util::basePair2ComplementBasePair(maxCountBp);

                currBridge.emplace_back(maxCountBp);
                currBridgeRevComp.emplace_front(maxCountBpCompl);
                ++currBridgeLen;

                movingWindow.emplace_back(maxCountBp);
                movingWindowRevComp.emplace_front(maxCountBpCompl);
            }
        }

        if(secondAnchor) {
            bool areEqual = true;
            for(my_size_t i = 0; i < kmerLen; i++) {
                BasePair currChar1 = secondAnchorKmerIt[i];
                BasePair currChar2 = currBridge[i];
                if(currChar1 != currChar2) {
                    areEqual = false;
                    break;
                }
            }

            if(areEqual) {
                toRet.emplace_back(currBridge);
            }
        } else {
            // Add it to the list if it has extended fully
            if(currBridgeLen == maxBranchLength) {
                toRet.emplace_back(currBridge);
            }
        }

        bridgesQueue.pop_front();
    }

    return toRet;
}

std::pair<my_size_t, std::deque<BasePair>> FMLRC::minEditDistanceBridge(deque<std::deque<BasePair>> &bestBridges, std::vector<BasePair>::iterator anchorKmerIt, my_size_t anchorKmerItLen, my_size_t kmerLen) {
    std::deque<BasePair>* toRet;
    my_size_t toRetMinEditDist;

    my_size_t minDistance = numeric_limits<my_size_t>::max();
    std::forward_list<std::deque<BasePair>*> minBridges;
    std::vector<my_size_t> minCntsTracker(bestBridges.size());

    my_size_t currDist;
    for(my_size_t i = 0; i < bestBridges.size(); i++) {
        auto& currBridge = bestBridges[i];
        currDist = Util::editDistanceBp(currBridge, anchorKmerIt, anchorKmerItLen);

        minCntsTracker[i] = currDist;

        if(currDist < minDistance) {
            minDistance = currDist;
        }
    }

    // Found the minimum edit distance in the bridges
    toRetMinEditDist = minDistance;

    // Do it again, now find all min edit distance bridges
    my_size_t minBridgesSize = 0;
    for(my_size_t i = 0; i < minCntsTracker.size(); i++) {
        my_size_t currCnt = minCntsTracker[i];
        if(currCnt == minDistance) {
            ++minBridgesSize;
            minBridges.emplace_front(&bestBridges[i]);
        }
    }

    // Check what we have found
    if(minBridgesSize == 1) {
        // Great, return that
        toRet = *minBridges.begin();
    } else {
        // Still more than one option - we have to chose one!
        // Look at kmer freqs in each bridge point!
        my_size_t maxCntSum = 0;
//        vector<my_size_t> minBridgesFreqs(minBridgesSize);
        my_size_t currBridgeSum;
        for(auto it = minBridges.begin(); it != minBridges.end(); it++) {
            vector<my_size_t> currBridgeCounts = std::move(this->allKmerFrequencies(**it, kmerLen).first);

            currBridgeSum = 0;
            for(my_size_t currCnt : currBridgeCounts) {
                currBridgeSum += currCnt;
            }

            if(currBridgeSum > maxCntSum) {
                maxCntSum = currBridgeSum;
                toRet = *it;
            }
        }
    }

    return make_pair(toRetMinEditDist, std::move(*toRet));
}

std::deque<BasePair>
FMLRC::seedAndExtendTail(std::vector<BasePair>::iterator anchorKmerIt, my_size_t anchorIndex, my_size_t kmerLen,
                         my_size_t branchLimit, my_size_t maxBranchLength, my_size_t currThreshold,
                         my_size_t weakRegionSize) {
    vector<deque<BasePair>> bridgeCandidates;

    // Short assemble here
    auto bestBridges = this->findBridges(anchorKmerIt, kmerLen, branchLimit, maxBranchLength, currThreshold, anchorKmerIt, false);
    if(bestBridges.empty()) {
        return {};
    }

    // Compare edit distance until the end of the graph!
    auto toRetBestBridge = std::move(this->minEditDistanceBridge(bestBridges, anchorKmerIt, weakRegionSize, kmerLen).second);

    // Cull if too large
    if(toRetBestBridge.size() > weakRegionSize) {
        toRetBestBridge.resize(weakRegionSize);
    }

    return toRetBestBridge;
}

my_float_t FMLRC::calculateAverage(vector<BasePair> &currLongRead, my_size_t myIndex, vector<my_float_t> &dest, my_size_t kmerLen,
                                   std::vector<my_size_t> *kmerCntPar) {
    std::vector<my_size_t> *currKmerCounts;
    std::vector<my_size_t> tmpVect;
    if(kmerCntPar == nullptr) {
        tmpVect = std::move(this->allKmerFrequencies(currLongRead, kmerLen).first);
        currKmerCounts = &tmpVect;
    } else {
        currKmerCounts = kmerCntPar;
    }

    // Calculation part
    my_float_t res = 0;

    for(my_size_t currCnt : (*currKmerCounts)) {
        res += currCnt;
    }

    res /= (*currKmerCounts).size();

    dest[myIndex] = res;
    return res;
}

void FMLRC::performCorrections(forward_list<SingleCorrection> &corrList, std::vector<BasePair>& currLongRead) {
    for(SingleCorrection& currCorr : corrList) {
        my_size_t currCorrIndex = currCorr.startIndex;
        deque<BasePair>& currCorrSeq = currCorr.corrBridge;

        for(BasePair currBp : currCorrSeq) {
            if(currCorrIndex < currLongRead.size()) {
                currLongRead[currCorrIndex++] = currBp;
            } else {
                currLongRead.emplace_back(currBp);
            }
        }
    }
}
