//
// Created by kikyy_99 on 15.12.22..
//

#ifndef SW_PROJECT_UTIL_H
#define SW_PROJECT_UTIL_H

#include <cstdint>
#include <string>
#include <vector>
#include <stdexcept>
#include <iostream>
#include <deque>

using BasePair_underlying_type = uint8_t;
enum class BasePair: BasePair_underlying_type {$ = 0, A = 1, C = 2, G = 3, N = 4, T = 5};
enum class Mode {Bwt, Corr, Unsupported};
/**
 * my_size_t represents the desired length of counter/index variables. Since uint32_t could not be long enough for some
 * cases of DNA and uint64_t should instead be used, this using/typedef construct allows fast and one-place switching
 * of the types
 */
using my_size_t = int32_t;
using my_float_t = double;

class Util {
public:
    inline static constexpr my_size_t MY_SIZE_T_BITWIDTH = sizeof(my_size_t) * 8;
    inline static constexpr my_size_t MY_SIZE_T_BYTES = sizeof(my_size_t);

    inline static constexpr my_size_t BASEPAIR_UNDERLYING_TYPE_BITWIDTH = sizeof(BasePair_underlying_type) * 8;
    inline static constexpr my_size_t BASEPAIR_UNDERLYING_TYPE_BYTES = sizeof(BasePair_underlying_type);

    inline static constexpr my_size_t MY_FLOAT_T_BITWIDTH = sizeof(my_float_t) * 8;
    inline static constexpr my_size_t MY_FLOAT_T_BYTES = sizeof(my_float_t);

    inline static constexpr int ALPHABET_SIZE =  6;
    inline static constexpr char ENDING_CHAR = '$';

    inline static constexpr double EPSILON = 1E-6;
    inline static constexpr uint16_t SIZEOF_BITS = 8;

    inline static void reportArgumentErrorAndExit() {
        std::cerr << "Invalid arguments!" << std::endl;
        exit(-1);
    }

    inline static BasePair char2BasePair(char in) {
        switch(in) {
            case 'A':
                return BasePair::A;
            case'C':
                return BasePair::C;
            case 'G':
                return BasePair::G;
            case 'N':
                return BasePair::N;
            case 'T':
                return BasePair::T;
            case '$':
                return BasePair::$;
            default:
                throw std::invalid_argument("Can't convert this character to BasePair!");
        }
    }

    inline static char basePair2Char(BasePair in) {
        switch(in) {
            case BasePair::A: return 'A';
            case BasePair::C: return 'C';
            case BasePair::G: return 'G';
            case BasePair::N: return 'N';
            case BasePair::T: return 'T';
            case BasePair::$: return '$';
            default:
                throw std::invalid_argument("Can't convert this BasePair to character!");
        }
    }

    inline static BasePair basePair2ComplementBasePair(BasePair in) {
        switch(in) {
            case BasePair::A:
                return BasePair::T;
            case BasePair::C:
                return BasePair::G;
            case BasePair::G:
                return BasePair::C;
            case BasePair::T:
                return BasePair::A;
            case BasePair::N:
                return BasePair::N;
            case BasePair::$:
                return BasePair::$;
            default:
                throw std::invalid_argument("Can't convert this BasePair to character!");
        }
    }

    inline static my_size_t editDistanceBp(std::deque<BasePair>& seq1,  std::vector<BasePair>::iterator seq2It, my_size_t kmerLen) {
        my_size_t m = seq1.size();
        my_size_t n = kmerLen;

        std::vector<my_size_t> dataRow1(m+1), dataRow2(m+1);
        std::vector<my_size_t> *firstRow = &dataRow1, *secondRow = &dataRow2;
        std::vector<my_size_t> *tmpRow = nullptr;

        BasePair currChar1, currChar2;

        // Fill 0th row first
        for(my_size_t i = 0; i <= m; i++) {
            (*firstRow)[i] = i;
        }
        std::fill(dataRow2.begin(), dataRow2.end(), 0);

        for(my_size_t i = 1; i <= n; ++i) {
            currChar2 = seq2It[i - 1];

            for(my_size_t j = 0; j <= m; ++j) {
                // Write to second row
                if(j == 0) [[unlikely]] {
                    (*secondRow)[j] = i;
                } else [[likely]] {
                    currChar1 = seq1[j - 1];
                    if(currChar1 == currChar2) {
                        (*secondRow)[j] = (*firstRow)[j - 1];
                    } else {
                        (*secondRow)[j] = 1 + std::min({(*firstRow)[j-1], (*firstRow)[j], (*secondRow)[j-1]});
                    }
                }
            }

            // Switch rows
            tmpRow = secondRow;
            secondRow = firstRow;
            firstRow = tmpRow;
        }

        return (*firstRow)[m];
    }

    template<typename T, typename V>
    inline static my_size_t editDistance(T seq1, T seq2) {
        my_size_t m = seq1.size(), n = seq2.size();

        std::vector<my_size_t> dataRow1(m+1), dataRow2(m+1);
        std::vector<my_size_t> *firstRow = &dataRow1, *secondRow = &dataRow2;
        std::vector<my_size_t> *tmpRow = nullptr;

        V currChar1, currChar2;

        // Fill 0th row first
        for(my_size_t i = 0; i <= m; i++) {
            (*firstRow)[i] = i;
        }
        std::fill(dataRow2.begin(), dataRow2.end(), 0);

        for(my_size_t i = 1; i <= n; ++i) {
            currChar2 = seq2[i - 1];

            for(my_size_t j = 0; j <= m; ++j) {
                // Write to second row
                if(j == 0) [[unlikely]] {
                    (*secondRow)[j] = i;
                } else [[likely]] {
                    currChar1 = seq1[j - 1];
                    if(currChar1 == currChar2) {
                        (*secondRow)[j] = (*firstRow)[j - 1];
                    } else {
                        (*secondRow)[j] = 1 + std::min({(*firstRow)[j-1], (*firstRow)[j], (*secondRow)[j-1]});
                    }
                }
            }

            // Switch rows
            tmpRow = secondRow;
            secondRow = firstRow;
            firstRow = tmpRow;
        }

        return (*firstRow)[m];
    };
};

#endif //SW_PROJECT_UTIL_H
