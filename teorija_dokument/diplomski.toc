\babel@toc {croatian}{}\relax 
\contentsline {chapter}{\numberline {1}Uvod}{1}{chapter.1}%
\contentsline {chapter}{\numberline {2}Sekvenciranje genoma}{5}{chapter.2}%
\contentsline {chapter}{\numberline {3}Sastavljanje genoma}{8}{chapter.3}%
\contentsline {section}{\numberline {3.1}De novo sastavljanje genoma}{9}{section.3.1}%
\contentsline {chapter}{\numberline {4}Hibridne metode ispravljanja}{11}{chapter.4}%
\contentsline {chapter}{\numberline {5}de Bruijn grafovi}{13}{chapter.5}%
\contentsline {chapter}{\numberline {6}FM-indeks}{15}{chapter.6}%
\contentsline {section}{\numberline {6.1}BWT}{16}{section.6.1}%
\contentsline {subsection}{\numberline {6.1.1}BWT i SA naivnom metodom konstrukcije}{17}{subsection.6.1.1}%
\contentsline {subsection}{\numberline {6.1.2}BWT i SA pomoću SA-IS algoritma}{18}{subsection.6.1.2}%
\contentsline {subsection}{\numberline {6.1.3}Algoritam konstrukcije BWT iz SA}{24}{subsection.6.1.3}%
\contentsline {section}{\numberline {6.2}FM-indeks u dubinu}{24}{section.6.2}%
\contentsline {chapter}{\numberline {7}FMLRC}{30}{chapter.7}%
\contentsline {section}{\numberline {7.1}Pregled s visoke razine apstrakcije}{30}{section.7.1}%
\contentsline {section}{\numberline {7.2}Proces ispravljanja s k-merom u dubinu}{34}{section.7.2}%
\contentsline {chapter}{\numberline {8}Analiza i rezultati}{45}{chapter.8}%
\contentsline {chapter}{\numberline {9}Zaključak}{57}{chapter.9}%
\contentsline {chapter}{Literatura}{59}{chapter*.36}%
