\begin{thebibliography}{14}
\providecommand{\natexlab}[1]{#1}
\providecommand{\url}[1]{\texttt{#1}}
\expandafter\ifx\csname urlstyle\endcsname\relax
  \providecommand{\doi}[1]{doi: #1}\else
  \providecommand{\doi}{doi: \begingroup \urlstyle{rm}\Url}\fi

\bibitem[ECo({\natexlab{a}})]{EColiLongReads}
E coli pacbio long read dataset, {\natexlab{a}}.
\newblock URL \url{https://www.ncbi.nlm.nih.gov/sra/DRX402465[accn}.

\bibitem[ECo({\natexlab{b}})]{EColiShortReads}
E coli illumina short read dataset, {\natexlab{b}}.
\newblock URL \url{https://www.ncbi.nlm.nih.gov/sra/SRX19125458[accn}.

\bibitem[edi()]{editDistanceGfg}
Geeks4geeks edit distance.
\newblock URL \url{https://www.geeksforgeeks.org/edit-distance-dp-5/}.

\bibitem[fin({\natexlab{a}})]{finnishBWT}
Helsinki bwt lecture, {\natexlab{a}}.
\newblock URL
  \url{https://www.cs.helsinki.fi/u/tpkarkka/opetus/12k/dct/lecture08.pdf}.

\bibitem[fin({\natexlab{b}})]{finnishInducedSorting}
Helsinki induced sorting lecture, {\natexlab{b}}.
\newblock URL
  \url{https://www.cs.helsinki.fi/u/tpkarkka/opetus/11s/spa/lecture11.pdf}.

\bibitem[gen()]{genomesOnlineNihNcbi}
Nih ncbi genomes online.
\newblock URL \url{https://www.ncbi.nlm.nih.gov/genome}.

\bibitem[scr()]{screwtapeZorkSAIS}
Screwtape zork sais.
\newblock URL \url{https://zork.net/~st/jottings/sais.html}.

\bibitem[Bauer et~al.(2011)Bauer, Cox, i Rosone]{lightweightBwtPpt}
Markus~J. Bauer, Anthony~J. Cox, i Giovanna Rosone.
\newblock Lightweight bwt construction for very large string collections.
\newblock 2011.
\newblock URL
  \url{https://pdfs.semanticscholar.org/67e4/40689c356b0403fe5bdd94a9a5ba6ffbc774.pdf}.

\bibitem[Ferragina i Manzini()]{fmIndexArticle}
Paolo Ferragina i Giovanni Manzini.
\newblock The fm-index: A compressed full-text index based on the bwt.
\newblock URL
  \url{http://archive.dimacs.rutgers.edu/Workshops/BWT/ferragina.pdf}.

\bibitem[Holt i
  McMillan(2014{\natexlab{a}})]{bwtOfLargeStringCollectionsViaMerging}
James Holt i Leonard McMillan.
\newblock Constructing burrows-wheeler transforms of large string collections
  via merging.
\newblock U \emph{Proceedings of the 5th ACM Conference on Bioinformatics,
  Computational Biology, and Health Informatics}, BCB '14, stranica 464–471,
  New York, NY, USA, 2014{\natexlab{a}}. Association for Computing Machinery.
\newblock ISBN 9781450328944.
\newblock \doi{10.1145/2649387.2649431}.
\newblock URL \url{https://doi.org/10.1145/2649387.2649431}.

\bibitem[Holt i
  McMillan(2014{\natexlab{b}})]{mergingOfMultiStringBwtsWithApplications}
James Holt i Leonard McMillan.
\newblock Merging of multi-string bwts with applications.
\newblock \emph{Bioinformatics}, 2014{\natexlab{b}}.
\newblock URL \url{https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4318930/}.

\bibitem[Langmead()]{bwtAndFmIndexPpt}
Ben Langmead.
\newblock Burrows-wheeler transform and fm index.
\newblock URL
  \url{https://www.cs.jhu.edu/~langmea/resources/lecture_notes/bwt_and_fm_index.pdf}.

\bibitem[Wang et~al.(2018)Wang, Holt, McMillan, i Jones]{fmlrcArticle}
Jeremy~R. Wang, James Holt, Leonard McMillan, i Corbin~D. Jones.
\newblock Fmlrc: Hybrid long read error correction using an fm-index.
\newblock \emph{BMC Bioinformatics}, 2018.
\newblock URL \url{UR - https://doi.org/10.1186/s12859-018-2051-3}.

\bibitem[Šikić i Domazet-Lošo(2013)]{bioinformatikaSkripta}
Mile Šikić i Mirjana Domazet-Lošo.
\newblock \emph{Bioinformatika - Skripta}.
\newblock Faculty of Electrical Engineering and Computing, University of
  Zagreb, 2013.
\newblock URL \url{https://www.fer.unizg.hr/predmet/bio1}.

\end{thebibliography}
