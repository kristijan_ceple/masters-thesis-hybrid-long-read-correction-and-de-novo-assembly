//
// Created by kikyy_99 on 20.01.23..
//

#ifndef SW_PROJECT_MASTERS_THESIS_FMLRC_H
#define SW_PROJECT_MASTERS_THESIS_FMLRC_H


#include <forward_list>
#include <thread>
#include <mutex>
#include <set>
#include <ranges>
#include <variant>
#include "Util.h"
#include "SA_IS_FMIndex.h"

class FMLRC {
private:
    struct SingleCorrection {
        my_size_t startIndex;
        my_size_t length;
        std::deque<BasePair> corrBridge;

        inline SingleCorrection(my_size_t startIndex, my_size_t length, std::deque<BasePair>&& corrBridge)
        : startIndex{startIndex}, length{length}, corrBridge{corrBridge} {}
    };

    enum class KmerType: uint8_t {WEAK = 0, SOLID = 1};
    inline static KmerType kmerType(my_size_t currKmerCount, my_size_t currThreshold) {
        if(currKmerCount < currThreshold) {
            return KmerType::WEAK;
        } else {
            return KmerType::SOLID;
        }
    }

    bool longKPass;

    Base_FMIndex *fmIndex = nullptr;
//    std::mutex fmIndexMutex;

    std::deque<std::vector<BasePair>>* longReadDataset;
    std::vector<std::vector<BasePair>> correctedLongReadDataset;
    std::vector<my_float_t> averageLongReadFrequenciesBefore;
    std::vector<my_float_t> averageLongReadFrequenciesAfter;

    my_float_t calculateAverage(std::vector<BasePair>& currLongRead, my_size_t myIndex, std::vector<my_float_t>& dest, my_size_t kmerLen, std::vector<my_size_t>* kmerCntPar = nullptr);

    /**
     * Number of long reads within the dataset
     */
    my_size_t longReadDatasetSize;

    my_size_t threadsNum;
    std::vector<std::jthread> threads;

    /**
     * The function a single thread worker executes. Basically, each thread gets assigned a number of long reads
     * that it has to correct. This thread will do jobs in range [longReadsBegin, londReadsEnd].
     *
     * @param longReadsBegin The start index of jobs to be done. Inclusive
     * @param londReadsEnd The end index of jobs to be done. Inclusive
     */
    static void threadWorker(my_size_t myID, my_size_t beginJobIndex, my_size_t jobsToDo, FMLRC* myObj);
    std::forward_list<SingleCorrection> onePass(std::vector<BasePair>& currLongRead, my_size_t kmerLen, my_size_t myIndex = 0, bool calcPreAvg = false);
    static void performCorrections(std::forward_list<SingleCorrection>& corrList, std::vector<BasePair>& currLongRead);

    void distributeJobsToThreads();

    template<typename T>
    inline std::pair<std::vector<my_size_t>, std::multiset<my_size_t>> allKmerFrequencies(T& toCountSeq, my_size_t kmerLen, T* toCountSeqRevCompOpt = nullptr, bool medianCalc = false, my_size_t currThreshold = 0) {
        std::vector<my_size_t> freqVect(0);
        std::multiset<my_size_t> orderedFreqs;

        if (toCountSeq.size() < kmerLen) {
            return std::make_pair(freqVect, orderedFreqs);
        }

        my_size_t toCompute = toCountSeq.size() - kmerLen + 1;
        freqVect.resize(toCompute);

        T newRevCompSeq(0);
        T* currRevCompSeq;

        if(toCountSeqRevCompOpt == nullptr) {
            newRevCompSeq = std::move(std::get<T>(FMLRC::reverseAndComplement<T>(toCountSeq)));
            currRevCompSeq = &newRevCompSeq;
        } else {
            currRevCompSeq = toCountSeqRevCompOpt;
        }

        my_size_t currKmerCount;
//        {
//            std::lock_guard<std::mutex> lg{this->fmIndexMutex};
            for (my_size_t i = 0; i < toCompute; i++) {
                currKmerCount = this->fmIndex->queryNum(toCountSeq.begin() + i, kmerLen);
                currKmerCount += this->fmIndex->queryNum(currRevCompSeq->begin() + (toCompute - 1 - i), kmerLen);

                freqVect[i] = currKmerCount;

                if (medianCalc) {
                    if(currKmerCount >= currThreshold) {
                        // Put the element where it belongs
                        orderedFreqs.emplace(currKmerCount);
                    }
                }
            }
//        }

        return make_pair(std::move(freqVect), std::move(orderedFreqs));
    }

    static inline my_float_t calculateMedianFreq(const std::multiset<my_size_t>& inData) {
        // Check empty case
        if(inData.empty()) {
            return 0;
        }

        // Find the middle, average them if there are multiple values
        my_float_t res = 0;
        my_size_t middleIndex = inData.size() / 2;

        auto it = inData.begin();
        std::advance(it, middleIndex);

        if(inData.size() % 2 == 0) {
            // Even number of data
            res += static_cast<my_float_t>(*(it--));
            res += static_cast<my_float_t>(*it);
            res *= 0.5;
        } else {
            // Odd number of data
            res = static_cast<my_float_t>(*it);
        }

        return res;
    }

    std::deque<BasePair>
    seedAndExtendHead(std::vector<BasePair>::iterator regionBeginIt, std::vector<BasePair>::iterator anchorKmerIt,
                      my_size_t anchorIndex, my_size_t kmerLen, my_size_t branchLimit, my_size_t maxBranchLength,
                      my_size_t currThreshold);
    std::deque<BasePair> seedAndExtendTail(std::vector<BasePair>::iterator anchorKmerIt, my_size_t anchorIndex, my_size_t kmerLen, my_size_t branchLimit, my_size_t maxBranchLength, my_size_t currThreshold, my_size_t weakRegionSize);
    std::vector<std::deque<BasePair>> seedAndBridge(std::vector<BasePair>::iterator firstAnchorKmerIt, std::vector<BasePair>::iterator secondAnchorKmerIt, my_size_t kmerLen, my_size_t branchLimit, my_size_t maxBranchLength, my_size_t currThreshold);

    std::deque<std::deque<BasePair>> findBridges(std::vector<BasePair>::iterator anchorKmer, my_size_t kmerLen, my_size_t branchLimit, my_size_t maxBranchLength, my_size_t currThreshold, std::vector<BasePair>::iterator secondAnchorKmerIt, bool secondAnchor = false);
    std::pair<my_size_t, std::deque<BasePair>> minEditDistanceBridge(std::deque<std::deque<BasePair>>& bestBridges, std::vector<BasePair>::iterator anchorKmerIt, my_size_t anchorKmerItLen, my_size_t kmerLen);

    /**
     * Used in region of X type to find the beginning of the region of Y type
     * Returns kmerCounts.size() if it cannot find the next region of the Y type
     * Y type is specified by the desiredType parameter
     *
     * @param kmerCounts Self-explanatory
     * @param currKmerIndex The index of the current region of the desired type's opposite type
     * @param currThreshold Self-explanatory
     * @return the index of the next region or kmerCounts.size() if not found
     */
    static inline my_size_t findTheNextKmerType(const std::vector<my_size_t> &kmerCounts, my_size_t currKmerIndex, my_size_t currThreshold, KmerType desiredKmerType) {
        my_size_t currKmerCount;
        KmerType currKmerType;
        ++currKmerIndex;

        while(currKmerIndex < kmerCounts.size()) {
            currKmerCount = kmerCounts[currKmerIndex];
//            currKmerType = FMLRC::kmerType(currKmerCount, currThreshold);
            currKmerType = currKmerCount < currThreshold ? KmerType::WEAK : KmerType::SOLID;
            if(currKmerType == desiredKmerType) [[unlikely]] {
                return currKmerIndex;
            } else [[likely]] {
                ++currKmerIndex;
            }
        }

        return kmerCounts.size();
    }

public:
    /**
     * Absolute minimum frequency required for a k-mer to be considered solid in the de Bruijn graph
     */
    const my_size_t PARAMETER_T;

    /**
     * Fraction of the median counts(m) required for a k-mer to be considered solid in the DBG,
     * and is used for the dynamic solid k-mer threshold calculation, t=max(T, F*m)
     */
    const my_float_t PARAMETER_F;

    /**
     * The branch limit factor that limits the amount of computation of a de Bruijn graph traversal,
     * and is used in the computation of the maximum branch limit, L = B * k
     */
    const my_size_t PARAMETER_B;

    /**
     * Short k-mer length
     */
    const my_size_t PARAMETER_k;

    /**
     * Long k-mer length
     */
    const my_size_t PARAMETER_K;

    /**
     * Number of rows which one SA sample will cover
     */
    const my_size_t FM_INDEX_PARAMETER_A;

    /**
     * Number of rows which one tally BWT checkpoint will cover
     */
    const my_size_t FM_INDEX_PARAMETER_B;

    /**
     * Controls the query result vector pre-allocated size.
     * Increase this parameter to decrease the vector pre-allocated size.
     * Check Base FM Index class for more info
     */
    const my_float_t SUFFIX_RESULT_RESERVATION_FACTOR;

    /**
     * This factor is applied to any 2-anchor point weak region bridge gaps to allow for insertions
     * These weak regions are located between two solid regions
     */
    const my_float_t MID_BRIDGE_BUFFER_FACTOR;

    /**
     * This factor is applied to any head/tail(means 1 anchor point - only on one side) bridge gap to allow for insertions
     * These weak regions are flanked by one solid region
     */
    const my_float_t PERIPHERY_BRIDGE_BUFFER_FACTOR;

    /**
     * Do not attempt to bridge gaps that are bigger than this
     */
    const my_size_t MAX_GAP_LENGTH;

    /**
     * Is used when we are bridging using a midpoint
     * If the seed-and-extend bridges done on this midpoint gap are not good enough,
     * then they are not corrected.
     *
     * This factor specifies the maximum edit distance ratio that a bridge needs to be below
     * of in order to be accepted as a correction
     */
    const my_float_t MID_POINT_BRIDGE_EDIT_DIST_FACTOR;

    const bool QUALITY_ASSURANCE = true;

    inline static constexpr my_size_t PARAMETER_T_DEFAULT = 5;
    inline static constexpr my_float_t PARAMETER_F_DEFAULT = 0.10;
    inline static constexpr my_size_t PARAMETER_B_DEFAULT = 4;
    inline static constexpr my_size_t PARAMETER_k_DEFAULT = 21;
    inline static constexpr my_size_t PARAMETER_K_DEFAULT = 59;
    inline static constexpr my_float_t BRANCH_BUFFER_FACTOR_DEFAULT = 1.3;
    inline static constexpr my_float_t PERIPHERY_BUFFER_FACTOR_DEFAULT = 1.05;
    inline static constexpr my_size_t MAX_GAP_LENGTH_DEFAULT = 10000;
    inline static constexpr my_float_t MIN_POINT_BRIDGE_EDIT_DIST_FACTOR_DEFAULT = 0.4;

    std::forward_list<std::vector<BasePair>> longReadsCorrected;
    bool resultReady = false;

    explicit FMLRC(  Base_FMIndex* shortReadFMIndex = nullptr, std::deque<std::vector<BasePair>>* longReadDataset = nullptr,
            my_size_t longReadDatasetSize = 0,
            my_size_t threadsNum = std::jthread::hardware_concurrency(),
            my_size_t parameterT = FMLRC::PARAMETER_T_DEFAULT, my_float_t parameterF = FMLRC::PARAMETER_F_DEFAULT,
            my_size_t parameterB = FMLRC::PARAMETER_B_DEFAULT, my_size_t parameter_k = FMLRC::PARAMETER_k_DEFAULT,
            my_size_t parameter_K = FMLRC::PARAMETER_K_DEFAULT, my_size_t fmIndexParameterA = Base_FMIndex::DEFAULT_A_PARAM,
            my_size_t fmIndexParameterB = Base_FMIndex::DEFAULT_B_PARAM, my_float_t branchBufferFactor = FMLRC::BRANCH_BUFFER_FACTOR_DEFAULT,
            my_float_t peripheryBufferFactor = FMLRC::PERIPHERY_BUFFER_FACTOR_DEFAULT, my_size_t maxGapLength = FMLRC::MAX_GAP_LENGTH_DEFAULT,
            my_float_t suffixResultReservationFactor = Base_FMIndex::DEFAULT_SUFFIX_RESULT_RESERVATION_FACTOR, my_float_t midPointBridgeEditDistFactor = FMLRC::MIN_POINT_BRIDGE_EDIT_DIST_FACTOR_DEFAULT)
    : fmIndex{shortReadFMIndex}, longReadDataset{longReadDataset},
      PARAMETER_T{parameterT}, PARAMETER_F{parameterF}, PARAMETER_B{parameterB}, PARAMETER_k{parameter_k}, PARAMETER_K{parameter_K},
      FM_INDEX_PARAMETER_A{fmIndexParameterA}, FM_INDEX_PARAMETER_B{fmIndexParameterB}, MAX_GAP_LENGTH{maxGapLength},
      threadsNum{threadsNum}, longReadDatasetSize{longReadDatasetSize}, MID_BRIDGE_BUFFER_FACTOR{branchBufferFactor},
      PERIPHERY_BRIDGE_BUFFER_FACTOR{peripheryBufferFactor}, SUFFIX_RESULT_RESERVATION_FACTOR{suffixResultReservationFactor},
      MID_POINT_BRIDGE_EDIT_DIST_FACTOR{midPointBridgeEditDistFactor}
    {
        this->longKPass = (this->PARAMETER_K > 0);
        this->longReadsCorrected.resize(this->longReadDatasetSize);
        this->correctedLongReadDataset.resize(longReadDatasetSize);

        this->averageLongReadFrequenciesBefore.resize(longReadDatasetSize);
        this->averageLongReadFrequenciesAfter.resize(longReadDatasetSize);
    }

    void execute();

    template<typename T>
    inline static std::variant<std::vector<BasePair>, std::deque<BasePair>> reverseAndComplement(T& toRevComp) {
        bool isDeque = std::is_same<T, std::deque<BasePair>>::value;

        my_size_t kmerLen;
        if(isDeque) {
            kmerLen = reinterpret_cast<std::deque<BasePair>&>(toRevComp).size();
            return FMLRC::reverseAndComplement(reinterpret_cast<std::deque<BasePair>&>(toRevComp).cbegin(), kmerLen);
        } else {
            kmerLen = reinterpret_cast<std::vector<BasePair>&>(toRevComp).size();
            return FMLRC::reverseAndComplement(reinterpret_cast<std::vector<BasePair>&>(toRevComp).cbegin(), kmerLen);
        }
    }

    template<typename T>
    inline static std::variant<std::vector<BasePair>, std::deque<BasePair>> reverseAndComplement(T toRevComp, my_size_t kmerLen) {
        std::variant<std::vector<BasePair>, std::deque<BasePair>> toRet;
        std::vector<BasePair>* toRetVect;
        std::deque<BasePair>* toRetDeque;

        bool isDequeIt = std::is_same<T, std::deque<BasePair>::const_iterator>::value;

        if(isDequeIt) {
            toRet = std::deque<BasePair>(kmerLen);
            toRetDeque = &std::get<std::deque<BasePair>>(toRet);
        } else {
            toRet = std::vector<BasePair>(kmerLen);
            toRetVect = &std::get<std::vector<BasePair>>(toRet);
        }

        for(my_size_t i = 0, j = kmerLen-1; i < kmerLen; i++,j--) {
            BasePair currBp = toRevComp[i];
            BasePair compBp = Util::basePair2ComplementBasePair(currBp);
            if(isDequeIt) {
                (*toRetDeque)[j] = compBp;
            } else {
                (*toRetVect)[j] = compBp;
            }
        }

        return toRet;
    }
};


#endif //SW_PROJECT_MASTERS_THESIS_FMLRC_H
