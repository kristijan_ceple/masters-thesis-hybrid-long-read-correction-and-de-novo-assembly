//
// Created by kikyy_99 on 15.12.22..
//

#ifndef SW_PROJECT_CIRCULARBUFFER_H
#define SW_PROJECT_CIRCULARBUFFER_H

#include <vector>

template<class T>
class CircularBuffer {
private:
    std::vector<T>& data;
    int head, tail;

public:
    CircularBuffer(std::vector<T>& data);
    void rotateLeft();
    void rotateRight();
};


#endif //SW_PROJECT_CIRCULARBUFFER_H
