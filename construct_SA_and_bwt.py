stringSrc = "ACCGTAATTT"

########################################################################################################################
########################################################################################################################
########################################################################################################################
stringSrc += "$"

rots = []
new_str_tmp = stringSrc

for i in range(len(stringSrc)):
	new_str_tmp = new_str_tmp[1:] + new_str_tmp[0]
	rots.append(new_str_tmp)

print("Rots = ")
for rot in rots:
	print(rot)

rots.sort()

print()
print("Sorted rots =")
for rot in rots:
	print(rot)

SA = []
parM = len(rots[0])
for rot in rots:
	dollar_substring_length = 1 + rot.find('$')
	SA.append(parM - dollar_substring_length)

bwt = [rot[-1] for rot in rots]

print("SA =")
print('{', end='')
print(', '.join(map(str, SA)), end='')
print('}')

print()
print("BWT =")
print('[', end='')
print(''.join(bwt), end='')
print(']')

print()
print("SA suffixes =")
for SA_rank, rot in zip(SA, rots):
	print(f"{SA_rank} => {stringSrc[SA_rank:]}")