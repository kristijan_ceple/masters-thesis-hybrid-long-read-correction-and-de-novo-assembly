//
// Created by kikyy_99 on 15.12.22..
//
#include "Util.h"
#include <ostream>

using namespace std;

std::ostream& operator<<(std::ostream& os, const vector<BasePair>& obj) {
    for(const BasePair& currBp : obj) {
        os << Util::basePair2Char(currBp);
    }
    return os;
}

template<class T>
concept integral_num = std::is_integral_v<T>;

template<class T>
concept floating_point_num = std::is_floating_point_v<T>;

std::ostream& operator<<(std::ostream& os, const vector<my_size_t>& obj) {
    os << '{';

    if(!obj.empty()) {
        os << obj[0];
    }

    for(my_size_t i = 1; i < obj.size(); i++) {
        os << ", " << obj[i];
    }
    os << '}';

    return os;
}

std::ostream& operator<<(std::ostream& os, const vector<my_float_t>& obj) {
    os << '{';

    if(!obj.empty()) {
        os << obj[0];
    }

    for(my_size_t i = 1; i < obj.size(); i++) {
        os << ", " << obj[i];
    }
    os << '}';

    return os;
}