//
// Created by kikyy_99 on 20.01.23..
//

#include <fstream>
#include <ranges>
#include <sstream>
#include <cstdlib>
#include <bitset>
#include "DataLoader.h"
using namespace std;

std::vector<BasePair> DataLoader::processInputString(string &in) {
    vector<BasePair> toRet{};
    toRet.reserve(in.size() + 1);       // We add +1 because of the '$'

    for(char currChar : in) {
        BasePair convertedChar = Util::char2BasePair(currChar);
        toRet.emplace_back(convertedChar);
    }

    return toRet;
}

std::vector<BasePair> DataLoader::getInputData(const std::string& filePath) {
    std::string loadedStr = DataLoader::readInputBasicFastaFile(filePath);
    return DataLoader::processInputString(loadedStr);
}

std::string DataLoader::readInputBasicFastaFile(const string &filePath) {
    ifstream readFile(filePath);
    if (!readFile.is_open()) {
        throw invalid_argument("File couldn't be opened and read! Check name, permissions, location, ....");
    }

    stringstream ss;
    string line;
    while(getline(readFile, line)) {
        if (line.empty()) {
            continue;
        } else if(line.starts_with(DataLoader::FASTA_COMMENT_CHAR)) {
            continue;
        }

        DataLoader::trimString(line);
        ss << std::move(line);
    }

    return ss.str();
}

std::vector<BasePair> DataLoader::getShortReadDataSet(const string &filePath, my_size_t basesUpperBound) {
    ifstream readFile(filePath);
    if (!readFile.is_open()) {
        throw invalid_argument("File couldn't be opened and read! Check name, permissions, location, ....");
    }

    vector<BasePair> toRet;
    toRet.reserve(basesUpperBound);

    string line;
    while(getline(readFile, line)) {
        if (line.starts_with(DataLoader::FASTA_COMMENT_CHAR) || line.starts_with(DataLoader::FASTA_LINE_INFO_CHAR)) {
            continue;
        }

        DataLoader::trimString(line);

        // Data
        for(char currChar : line) {
            BasePair convertedChar = Util::char2BasePair(currChar);
            toRet.emplace_back(convertedChar);
        }
    }

    return toRet;
}

std::pair<std::deque<std::vector<BasePair>>, my_size_t>
DataLoader::getLongReadDataSet(const string &filePath) {
    ifstream readFile(filePath);
    if (!readFile.is_open()) {
        throw invalid_argument("File couldn't be opened and read! Check name, permissions, location, ....");
    }

    deque<vector<BasePair>> toRet;

    string line;

    my_size_t currLength;
    my_size_t longReadsCount = 0;
    vector<BasePair> currLineData;
    while(getline(readFile, line)) {
        if (line.starts_with(DataLoader::FASTA_COMMENT_CHAR)) {
            continue;
        }

        DataLoader::trimString(line);

        if(line.starts_with(DataLoader::FASTA_LINE_INFO_CHAR)) {
            // Next line info
            line = line.substr(line.find_last_of('=') + 1);
            currLength = atoll(line.c_str());
        } else {
            ++longReadsCount;
            currLineData.reserve(currLength);

            // Data
            for(char currChar : line) {
                BasePair convertedChar = Util::char2BasePair(currChar);
                currLineData.emplace_back(convertedChar);
            }

            toRet.emplace_back(std::move(currLineData));
            currLineData = {};
        }
    }

    return make_pair(std::move(toRet), longReadsCount);
}

void DataLoader::writeFMIndexToDisk(Base_FMIndex &fmIndex, const string &outputFilePath) {
    ofstream writeFile(outputFilePath, ios::out | ios::binary);
    if (!writeFile) {
        throw invalid_argument("File couldn't be opened and read! Check name, permissions, location, ....");
    }

    // Write bwt in RLE format
    DataLoader::bwtRleEncode(fmIndex.bwt, writeFile);
    my_size_t tmp;

    // Then write cumulative counts
    tmp = fmIndex.cumulativeCounts.size();
    writeFile.write(reinterpret_cast<char*>(&tmp), Util::MY_SIZE_T_BYTES);
    for(my_size_t currNum : fmIndex.cumulativeCounts) {
        writeFile.write(reinterpret_cast<char*>(&currNum), Util::MY_SIZE_T_BYTES);
    }

    // Write SA Samples
    tmp = fmIndex.SASamples.size();
    writeFile.write(reinterpret_cast<char*>(&tmp), Util::MY_SIZE_T_BYTES);
    for(my_size_t currNum : fmIndex.SASamples) {
        writeFile.write(reinterpret_cast<char*>(&currNum), Util::MY_SIZE_T_BYTES);
    }

    // Write occ tally
    tmp = fmIndex.occTally.size();
    writeFile.write(reinterpret_cast<char*>(&tmp), Util::MY_SIZE_T_BYTES);
    for(auto& currArr : fmIndex.occTally) {
        tmp = currArr.size();
        writeFile.write(reinterpret_cast<char*>(&tmp), Util::MY_SIZE_T_BYTES);
        for(my_size_t currNum : currArr) {
            writeFile.write(reinterpret_cast<char*>(&currNum), Util::MY_SIZE_T_BYTES);
        }
    }


    // Write parameterM, PARAMETER_A, PARAMETER_B, PARAMETER_B_HALF, PARAMETER_B_RATIO, PARAMETER_A_RATIO, suffixResultReservationFactor
    writeFile.write(reinterpret_cast<const char*>(&fmIndex.parameterM), Util::MY_SIZE_T_BYTES);
    writeFile.write(reinterpret_cast<const char*>(&fmIndex.PARAMETER_A), Util::MY_SIZE_T_BYTES);
    writeFile.write(reinterpret_cast<const char*>(&fmIndex.PARAMETER_B), Util::MY_SIZE_T_BYTES);
    writeFile.write(reinterpret_cast<const char*>(&fmIndex.PARAMETER_B_HALF), Util::MY_SIZE_T_BYTES);
    writeFile.write(reinterpret_cast<const char*>(&fmIndex.PARAMETER_B_RATIO), Util::MY_FLOAT_T_BYTES);
    writeFile.write(reinterpret_cast<const char*>(&fmIndex.PARAMETER_A_RATIO), Util::MY_FLOAT_T_BYTES);
    writeFile.write(reinterpret_cast<char*>(&fmIndex.suffixResultReservationFactor), Util::MY_FLOAT_T_BYTES);
}

void DataLoader::bwtRleEncode(const vector<BasePair> &bwt, ofstream &ofs) {
    my_size_t tmp = bwt.size();
    ofs.write(reinterpret_cast<char*>(&tmp), Util::MY_SIZE_T_BYTES);

    my_size_t currCnt = 1, currIndex = 1;
    BasePair prevChar = bwt[0], currChar;
    do {
        currChar = bwt[currIndex];

        if(currChar == prevChar) {
            ++currCnt;
        } else {
            // Write previous character -> <char><cnt>
            ofs.put(static_cast<BasePair_underlying_type>(prevChar));

            ofs.write(reinterpret_cast<char*>(&currCnt), Util::MY_SIZE_T_BYTES);

            // Update count for current character
            currCnt = 1;
        }

        ++currIndex;
        prevChar = currChar;
    } while(currIndex < bwt.size());

    // Write last character
    ofs.put(static_cast<BasePair_underlying_type>(currChar));

    ofs.write(reinterpret_cast<char*>(&currCnt), Util::MY_SIZE_T_BYTES);
}

std::unique_ptr<Base_FMIndex> DataLoader::loadFMIndexFromDisk(const string &inputFilePath) {
    unique_ptr<Base_FMIndex> toRet = make_unique<Base_FMIndex>();
    my_size_t tmpInt;

    ifstream readFile(inputFilePath, ios::in | ios::binary);
    if (!readFile) {
        throw invalid_argument("File couldn't be opened and read! Check name, permissions, location, ....");
    }

    // Read bwt in RLE format
    toRet->bwt = DataLoader::bwtRleDecode(readFile);

    // Then read cumulative counts
    readFile.read(reinterpret_cast<char*>(&tmpInt), Util::MY_SIZE_T_BYTES);
    for(my_size_t i = 0; i < tmpInt; i++) {
        readFile.read(reinterpret_cast<char*>(&toRet->cumulativeCounts[i]), Util::MY_SIZE_T_BYTES);
    }

    // Read SA Samples
    readFile.read(reinterpret_cast<char*>(&tmpInt), Util::MY_SIZE_T_BYTES);
    toRet->SASamples.resize(tmpInt);
    for(my_size_t i = 0; i < tmpInt; i++) {
        readFile.read(reinterpret_cast<char*>(&toRet->SASamples[i]), Util::MY_SIZE_T_BYTES);
    }

    // Read occ tally
    readFile.read(reinterpret_cast<char*>(&tmpInt), Util::MY_SIZE_T_BYTES);
    toRet->occTally.resize(tmpInt);
    for(my_size_t i = 0; i < toRet->occTally.size(); i++) {
        readFile.read(reinterpret_cast<char*>(&tmpInt), Util::MY_SIZE_T_BYTES);
        for(my_size_t j = 0; j < tmpInt; j++) {
            readFile.read(reinterpret_cast<char*>(&toRet->occTally[i][j]), Util::MY_SIZE_T_BYTES);
        }
    }

    // Read parameterM, PARAMETER_A, PARAMETER_B, PARAMETER_B_HALF, PARAMETER_B_RATIO, PARAMETER_A_RATIO, suffixResultReservationFactor
    readFile.read(reinterpret_cast<char*>(&toRet->parameterM), Util::MY_SIZE_T_BYTES);
    readFile.read(reinterpret_cast<char*>(&toRet->PARAMETER_A), Util::MY_SIZE_T_BYTES);
    readFile.read(reinterpret_cast<char*>(&toRet->PARAMETER_B), Util::MY_SIZE_T_BYTES);
    readFile.read(reinterpret_cast<char*>(&toRet->PARAMETER_B_HALF), Util::MY_SIZE_T_BYTES);

    readFile.read(reinterpret_cast<char*>(&toRet->PARAMETER_B_RATIO), Util::MY_FLOAT_T_BYTES);
    readFile.read(reinterpret_cast<char*>(&toRet->PARAMETER_A_RATIO), Util::MY_FLOAT_T_BYTES);
    readFile.read(reinterpret_cast<char*>(&toRet->suffixResultReservationFactor), Util::MY_FLOAT_T_BYTES);

    toRet->constructedBwt = true;
    toRet->constructedFMIndex = true;

    return toRet;
}

std::vector<BasePair> DataLoader::bwtRleDecode(ifstream &bwtFile) {
    std::vector<BasePair> toRet;

    my_size_t tmp;
    BasePair bpTmp;
    bwtFile.read(reinterpret_cast<char*>(&tmp), Util::MY_SIZE_T_BYTES);

    toRet.resize(tmp);

    my_size_t i = 0;
    my_size_t totalIndex = 0;
    while(totalIndex < toRet.size()) {
        // Read <char><cnt> format
        bwtFile.read(reinterpret_cast<char*>(&bpTmp), Util::BASEPAIR_UNDERLYING_TYPE_BYTES);
        bwtFile.read(reinterpret_cast<char*>(&tmp), Util::MY_SIZE_T_BYTES);

        for(my_size_t j = 0; j < tmp; j++) {
            toRet[totalIndex++] = static_cast<BasePair>(bpTmp);
        }
    }

    return toRet;
}

void
DataLoader::writeLongReadDatasetToDisk(deque<std::vector<BasePair>> &longReadDataset, const string &outputFilePath) {
    ofstream writeFile(outputFilePath, ios::out);
    if (!writeFile) {
        throw invalid_argument("File couldn't be opened and read! Check name, permissions, location, ....");
    }

    my_size_t currIndex = 0;
    for(auto& currData : longReadDataset) {
        writeFile << "@Name " << currIndex << " length=" << currData.size() << endl;

        for(BasePair currBp : currData) {
            writeFile << Util::basePair2Char(currBp);
        }

        writeFile << endl;
    }
}
