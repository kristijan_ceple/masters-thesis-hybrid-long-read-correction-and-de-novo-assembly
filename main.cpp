#include <iostream>
#include "SA_IS_FMIndex.h"
#include "DataLoader.h"
#include "FMLRC.h"
#include <chrono>
using namespace std;
using namespace std::chrono;

std::ostream& operator<<(std::ostream& os, const vector<BasePair>& obj);
std::ostream& operator<<(std::ostream& os, const vector<my_size_t>& obj);

void bwtProgram(int argc, char* argv[]) {
    if(argc != 4) {
        Util::reportArgumentErrorAndExit();
    }

    string inputFile = argv[2];
    string outputFile = argv[3];

    vector<BasePair> shortReads = DataLoader::getShortReadDataSet(inputFile);
    SA_IS_FMIndex saIsFMIndex{&shortReads};
    saIsFMIndex.constructFMIndex();

    DataLoader::writeFMIndexToDisk(saIsFMIndex, outputFile);
}

void corrProgram(int argc, char* argv[]) {
    if(argc != 5) {
        Util::reportArgumentErrorAndExit();
    }

    string fmIndexInput = argv[2];
    string inputLongDs = argv[3];
    string outputLongCorrDs = argv[4];

    auto longReadData = DataLoader::getLongReadDataSet(inputLongDs);
    unique_ptr<Base_FMIndex> fmIndex = DataLoader::loadFMIndexFromDisk(fmIndexInput);

    cout << "Loaded FM Index!" << endl;
//    cout << *fmIndex << endl;

    FMLRC fmlrc{fmIndex.get(), &longReadData.first, longReadData.second};
    cout << "Starting FMLRC!" << endl;
    fmlrc.execute();

    cout << "FMLRC done! Writing results to disk!" << endl;

    // Time to write results to disk
    DataLoader::writeLongReadDatasetToDisk(longReadData.first, outputLongCorrDs);
}

int main(int argc, char* argv[]) {
    auto start = high_resolution_clock::now();

    if(argc < 2) {
        Util::reportArgumentErrorAndExit();
    }

    string tmp;
    tmp = argv[1];
    Mode currMode = Mode::Unsupported;
    if(tmp == "-bwt") {
        currMode = Mode::Bwt;
    } else if(tmp == "-corr") {
        currMode = Mode::Corr;
    } else {
        Util::reportArgumentErrorAndExit();
    }

    if(currMode == Mode::Bwt) {
        bwtProgram(argc, argv);
    } else if(currMode == Mode::Corr) {
        corrProgram(argc, argv);
    } else {
        throw logic_error("Unsupported mode encountered!");
    }

    auto end = high_resolution_clock::now();
    cout << "Execution time: " << duration_cast<nanoseconds>(end-start).count() * 1E-9  << "s" << endl;

    return 0;
}
